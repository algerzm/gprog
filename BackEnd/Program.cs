﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace GProGApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var secretKey = Environment.GetEnvironmentVariable("LLAVE_SECRETA");
            if (string.IsNullOrEmpty(secretKey))
            {
                throw new ArgumentNullException(nameof(secretKey), "LLAVE_SECRETA environment variable is not set.");
            }

            DotNetEnv.Env.Load();
            
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseUrls(GetHerokuUrl())
                .UseEnvironment("Development")
                .UseIISIntegration()
                .UseStartup<Startup>();
        
        private static string GetHerokuUrl()
        {
            var port = Environment.GetEnvironmentVariable("PORT") ?? "4005";
            return $"http://*:{port}";
        }
    }
}
