﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.HttpOverrides;
using GProGApi.Hubs;
using Microsoft.Extensions.Hosting;
using Amazon.S3;
using Amazon.Extensions.NETCore.Setup;
using Amazon;
using Amazon.Runtime;
using DotNetEnv;

namespace GProGApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var awsAccessKey = Environment.GetEnvironmentVariable("AWS_ACCESS_KEY_ID");
            var awsSecretKey = Environment.GetEnvironmentVariable("AWS_SECRET_ACCESS_KEY");
            var awsRegion = Environment.GetEnvironmentVariable("AWS_REGION");
            var bucketName = Environment.GetEnvironmentVariable("AWS_BUCKET_NAME");

            Console.WriteLine($"AWS_ACCESS_KEY_ID: {awsAccessKey}");
            Console.WriteLine($"AWS_SECRET_ACCESS_KEY: {awsSecretKey}");
            Console.WriteLine($"AWS_REGION: {awsRegion}");
            Console.WriteLine($"AWS_BUCKET_NAME: {bucketName}");

            if (string.IsNullOrEmpty(awsAccessKey) || string.IsNullOrEmpty(awsSecretKey) || string.IsNullOrEmpty(awsRegion) || string.IsNullOrEmpty(bucketName))
            {
                throw new ArgumentNullException("One or more AWS configuration values are missing.");
            }

            var credentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
            var config = new AmazonS3Config
            {
                RegionEndpoint = Amazon.RegionEndpoint.GetBySystemName(awsRegion)
            };

            var s3Client = new AmazonS3Client(credentials, config);
            services.AddSingleton<IAmazonS3>(s3Client);

            // requires using Microsoft.Extensions.Options
            services.Configure<GProGDatabaseSettings>(
                Configuration.GetSection(nameof(GProGDatabaseSettings)));

            services.AddSignalR();

            services.AddSingleton<IGProGDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<GProGDatabaseSettings>>().Value);

            services.AddSingleton<UserService>();
            services.AddSingleton<IngredientService>();
            services.AddSingleton<SupplierService>();
            services.AddSingleton<ProjectService>();
            services.AddSingleton<FixedCostService>();
            services.AddSingleton<RecipeService>();
            services.AddSingleton<Utilities>();
            services.AddSingleton<SubRecipeService>();
            services.AddSingleton<ConsumableService>();
            services.AddSingleton<ServiceCostService>();
            services.AddSingleton<NotificationService>();
            services.AddSingleton<CatalogService>();
            services.AddSingleton<ProfitService>();
            services.AddSingleton<ChatService>();
            services.AddSingleton<CategoryService>();

            // Verificar que la variable de entorno LLAVE_SECRETA esté presente
            var secretKey = Configuration["LLAVE_SECRETA"];
            if (string.IsNullOrEmpty(secretKey))
            {
                throw new ArgumentNullException(nameof(secretKey), "LLAVE_SECRETA configuration value is not set.");
            }

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(
                options => options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "saconsultoria.com",
                    ValidAudience = "saconsultoria.com",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["LLAVE_SECRETA"])),
                    ClockSkew = TimeSpan.Zero
                });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<NotificationHub>("/notificationHub");
                endpoints.MapHub<MenuHub>("/menuHub");
                endpoints.MapHub<ChatHub>("/chatHub");
            });
        }
    }
}
