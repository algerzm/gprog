﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class IngredientInRecipe
    {
        public Int32 idIngrediente { get; set; }

        public double cantidad { get; set; }

        public Int32 unidadMedida { get; set; }

        public Int32 tipoReceta { get; set; }
    }
}
