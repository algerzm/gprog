﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class RecipeWithNames
    {
        public Int32 Id { get; set; }
        
        public string nombre { get; set; }

        public string tecnica { get; set; }

        public string tiempoPreparacion { get; set; }

        public string procedimiento { get; set; }

        public string montaje { get; set; }

        public string imagen { get; set; }

        public List<IngredientInRecipeName> ingredientes { get; set; }

        public double costoTotal { get; set; }

        public double precio { get; set; }

        public Int32 tipoReceta { get; set; }

        public Int32 costoServicio { get; set; }

        public Int32 idProyecto { get; set; }

        public string cuberteria { get; set; }

        public string categoria { get; set; }
    }
}
