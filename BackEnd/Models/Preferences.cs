﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class Preferences
    {
        public string idioma { get; set; }

        public string tema { get; set; }

        public bool notificaciones { get; set; }
    }
}
