﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GProGApi.Models
{
    public class Profit
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("mes")]
        public string fecha { get; set; }

        public double ganancia { get; set; }

        public Int32 idProyecto { get; set; }
    }
}
