﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class ChangePasword
    {
        public string password { get; set; }

        public string newPassword { get; set; }
    }
}
