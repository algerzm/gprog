﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class IngredientInRecipeName
    {
        public Int32 idIngrediente { get; set; }

        public string nombre { get; set; }

        public double cantidad { get; set; }

        public string unidadMedida { get; set; }

        public Int32 tipoReceta { get; set; }
    }
}
