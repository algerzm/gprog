﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class ProfilePicture
    {
        public string base64 { get; set; }
    }
}
