﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class MeanCosts
    {
        public double ticketPromedio { get; set; }

        public double costoVariable { get; set; }

        public double utilidadBruta { get; set; }

        public double costosFijos { get; set; }

        public double unidadesRequeridas { get; set; }

        public double ingresoUnidades { get; set; }
    }
}
