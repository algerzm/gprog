﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class UserToProject
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("idProyecto")]
        public Int32 idProyecto { get; set; }

        public Int32 idUsuario { get; set; }
    }
}
