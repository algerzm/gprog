﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class ProjectWithUsers
    {
        public ProjectWithUsers()
        {
            usuarios = new List<Users>();
        }

        public Int32 Id { get; set; }
        
        public string nombre { get; set; }

        public int duracion { get; set; }

        public string imagen { get; set; }

        public string descripcion { get; set; }

        public bool archivado { get; set; }

        public string infoCliente { get; set; }

        public List<Users> usuarios { get; set; }
    }
}
