﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class SearchIngredientName
    {
        public string nombre { get; set; }

        public Int32 idProyecto { get; set; }
    }
}
