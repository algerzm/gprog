﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GProGApi.Models
{
    public class Ingredient
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("nombre")]
        public string nombre { get; set; }

        public double precioPresentacion { get; set; }

        public double presentacion { get; set; }

        public Int32 unidadMedida { get; set; }

        public double precioUnitario { get; set; }

        public int merma { get; set; }

        public Int32 idProyecto { get; set; }
    }
}
