﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class ListOfIngInRecipe
    {
        public List<IngredientInRecipe> ingredientes { get; set; }
    }
}
