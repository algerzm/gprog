﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class IncomeDistribuition
    {
        public double ingresoVenta { get; set; }

        public double costosFijos { get; set; }

        public double costosVariables { get; set; }

        public double utilidadNeta { get; set; }
    }
}
