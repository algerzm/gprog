﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GProGApi.Models
{
    public class Consumable
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("nombre")]
        [BsonRequired]
        public string nombre { get; set; }

        [BsonRequired]
        public double costo { get; set; }

        [BsonRequired]
        public Int32 idProyecto { get; set; }
    }
}
