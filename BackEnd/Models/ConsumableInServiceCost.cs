﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GProGApi.Models
{
    public class ConsumableInServiceCost
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        public Int32 idInsumo { get; set; }

        public Int32 idCostoServicio { get; set; }
    }
}
