﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class BalancePoint
    {
        public BalancePoint()
        {
            precioVenta = new List<double>();
            unidades = new List<double>();
            ingresoTotal = new List<double>();
            costosFijos = new List<double>();
            costoVariableUnitario = new List<double>();
            costoVariableTotal = new List<double>();
            costoTotal = new List<double>();
            utilidadNeta = new List<double>();
        }

        public List<double> precioVenta { get; set; }

        public List<double> unidades { get; set; }

        public List<double> ingresoTotal { get; set; }

        public List<double> costosFijos { get; set; }

        public List<double> costoVariableUnitario { get; set; }

        public List<double> costoVariableTotal { get; set; }

        public List<double> costoTotal { get; set; }

        public List<double> utilidadNeta { get; set; }
    }
}
