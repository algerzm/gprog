﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GProGApi.Models
{
    public class Notification
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("titulo")]
        public string titulo { get; set; }

        public string mensaje { get; set; }

        public DateTime hora { get; set; }

        public Int32 idUsuario { get; set; }
    }
}
