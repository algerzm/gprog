﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class SubRecipe
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("nombre")]
        [BsonRequired]
        public string nombre { get; set; }

        public string tecnica { get; set; }

        public string tiempoPreparacion { get; set; }

        public string procedimiento { get; set; }

        public string montaje { get; set; }

        public string imagen { get; set; }

        public List<IngredientInRecipe> ingredientes { get; set; }

        [BsonRequired]
        public double unidadesProducidas { get; set; }

        public double costoTotal { get; set; }

        public double costoSubrecetaUnidad { get; set; }

        [BsonRequired]
        public Int32 idProyecto { get; set; }
    }
}
