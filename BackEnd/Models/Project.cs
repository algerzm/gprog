﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GProGApi.Models
{
    public class Project
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("nombre")]
        public string nombre { get; set; }

        public int duracion { get; set; }

        public string imagen { get; set; }

        public string descripcion { get; set; }

        public bool archivado { get; set; }

        public string infoCliente { get; set; }
    }
}
