﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class ChatWithUsers
    {
        public string mensaje { get; set; }

        public Int32 idUsuario { get; set; }

        public Users usuario { get; set; }

        public DateTime fecha { get; set; }
    }
}
