﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace GProGApi.Models
{
    public class ServiceCost
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("nombre")]
        [BsonRequired]
        public string nombre { get; set; }

        public string descripcion { get; set; }

        public double costo { get; set; }
        [BsonRequired]
        public Int32 idProyecto { get; set; }
    }
}
