﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GProGApi.Models
{
    public class Users
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("correo")]
        public string correo { get; set; }

        public string nombre { get; set; }

        public string password { get; set; }

        public int tipousuario { get; set; }

        public DateTime fechaAlta { get; set; }

        public Preferences preferencias { get; set; }
    }
}
