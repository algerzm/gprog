﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GProGApi.Models
{
    public class Recipe
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int32)]
        public Int32 Id { get; set; }

        [BsonElement("nombre")]
        [BsonRequired]
        public string nombre { get; set; }
        
        public string tecnica { get; set; }
        
        public string tiempoPreparacion { get; set; }

        public string procedimiento { get; set; }

        public string montaje { get; set; }

        public string imagen { get; set; }

        public List<IngredientInRecipe> ingredientes { get; set; }

        public double costoTotal { get; set; }

        public double precio { get; set; }

        public Int32 tipoReceta { get; set; }

        public Int32 costoServicio { get; set; }
        [BsonRequired]
        public Int32 idProyecto { get; set; }

        public string cuberteria { get; set; }

        public Int32 categoria { get; set; }

    }
}
