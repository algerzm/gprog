﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class DeleteProject
    {
        public int idProyecto { get; set; }

        public int idUsuario { get; set; }

        public string password { get; set; }
    }
}
