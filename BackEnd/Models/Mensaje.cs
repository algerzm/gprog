﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class Mensaje
    {
        public string mensaje { get; set; }

        public Int32 idUsuario { get; set; }

        public DateTime fecha { get; set; }
    }
}
