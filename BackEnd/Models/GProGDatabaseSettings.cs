﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class GProGDatabaseSettings : IGProGDatabaseSettings
    {
        public string UsersCollectionName { get; set; }
        public string IngredientsCollectionName { get; set; }
        public string SuppliersCollectionName { get; set; }
        public string ProjectsCollectionName { get; set; }
        public string FixedCostsCollectionName { get; set; }
        public string UsuariosProyectosCollectionName { get; set; }
        public string RecipesCollectionName { get; set; }
        public string SubRecipesCollectionName { get; set; }
        public string ConsumablesCollectionName { get; set; }
        public string ServicesCostsCollectionName { get; set; }
        public string ConsumableServicesCostsCollectionName { get; set; }
        public string NotificationsCollectionName { get; set; }
        public string NotificationsControlCollectionName { get; set; }
        public string TypesOfRecipesCollectionName { get; set; }
        public string MeasureUnitsCollectionName { get; set; }
        public string ProfitsCollectionName { get; set; }
        public string RolesCollectionName { get; set; }
        public string ChatsCollectionName { get; set; }
        public string CategoriesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
    //https://docs.microsoft.com/es-es/aspnet/core/tutorials/first-mongo-app?view=aspnetcore-3.1&tabs=visual-studio
    public interface IGProGDatabaseSettings
    {
        string UsersCollectionName { get; set; }
        string IngredientsCollectionName { get; set; }
        string SuppliersCollectionName { get; set; }
        string ProjectsCollectionName { get; set; }
        string FixedCostsCollectionName { get; set; }
        string UsuariosProyectosCollectionName { get; set; }
        string RecipesCollectionName { get; set; }
        string SubRecipesCollectionName { get; set; }
        string ConsumablesCollectionName { get; set; }
        string ServicesCostsCollectionName { get; set; }
        string ConsumableServicesCostsCollectionName { get; set; }
        string NotificationsCollectionName { get; set; }
        string NotificationsControlCollectionName { get; set; }
        string TypesOfRecipesCollectionName { get; set; }
        string MeasureUnitsCollectionName { get; set; }
        string ProfitsCollectionName { get; set; }
        string RolesCollectionName { get; set; }
        string ChatsCollectionName { get; set; }
        string CategoriesCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
