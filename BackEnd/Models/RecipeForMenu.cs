﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Models
{
    public class RecipeForMenu
    {
        public Int32 idReceta { get; set; }

        public string nombre { get; set; }

        public Int32 tipoReceta { get; set; }

        public double precio { get; set; }

        public double costoVariable { get; set; }

        public double costoFijo { get; set; }

        public double costoTotal { get; set; }

        public double utilidad { get; set; }

        public double porcentajeUtilidad { get; set; }
    }
}
