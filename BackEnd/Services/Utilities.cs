﻿using GProGApi.Services;
using GProGApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace GProGApi.Services
{
    public class Utilities
    {
        private readonly RecipeService _recipeService;
        private readonly SubRecipeService _subRecipeService;
        private readonly IngredientService _ingredientService;
        private readonly ProjectService _projectService;
        private readonly ServiceCostService  _serviceCost;
        private readonly ConsumableService _consumableService;
        private readonly FixedCostService _fixedCostService;
        private readonly ProfitService _profitService;
        private readonly CategoryService _categoryService;
        private readonly string baseUrl;

        public Utilities(SubRecipeService subRecipeService, IngredientService ingredientService, ProjectService projectService, RecipeService recipeService, ServiceCostService serviceCost, ConsumableService consumableService, FixedCostService fixedCostService, ProfitService profitService, CategoryService categoryService)
        {
            _subRecipeService = subRecipeService;
            _ingredientService = ingredientService;
            _projectService = projectService;
            _recipeService = recipeService;
            _serviceCost = serviceCost;
            _consumableService = consumableService;
            _fixedCostService = fixedCostService;
            _profitService = profitService;
            _categoryService = categoryService;
            
            Globals global = new Globals();
            baseUrl = global.getBaseUrl();
        }

        public dynamic getIngredient(int id, int tipoReceta)
        {
            Object ing = null;

            switch (tipoReceta)
            {
                case 1:
                    ing = _recipeService.Get(id);
                    break;
                case 2:
                    ing = _subRecipeService.Get(id);
                    break;
                case 5:
                    ing = _ingredientService.Get(id);
                    break;
                default:
                    ing = null;
                    break;
            }

            return ing;
        }

        public double CalculateCost(List<IngredientInRecipe> ingredientes)
        {
            double costo = 0;
            
            for (int i=0; i < ingredientes.Count; i++)
            {
                var ingredient = getIngredient(ingredientes[i].idIngrediente, ingredientes[i].tipoReceta);
                if(ingredientes[i].tipoReceta == 2)
                {
                    costo = costo + (ingredient.costoSubrecetaUnidad * ingredientes[i].cantidad);
                }
                else if(ingredientes[i].tipoReceta == 5)
                {
                    costo = costo + (ingredient.precioUnitario * ingredientes[i].cantidad);
                }
            }

            return costo;
        }

        //Actualiza costo de receta y regresa la receta actualizada
        public Recipe UpdateRecipeCosts(Recipe recipe)
        {
            double costoTotal = 0;
            //Falta agregar el costo de servicio
            if (recipe.ingredientes.Count > 0)
            {
                double serviceCost = 0;
                if (recipe.costoServicio != 0)
                {
                    var serviceCostObject = _serviceCost.Get(recipe.costoServicio);
                    if (serviceCostObject != null)
                    {
                        serviceCost = serviceCostObject.costo;
                    }
                }
                
                costoTotal = CalculateCost(recipe.ingredientes) + serviceCost;
            }
            
            recipe.costoTotal = costoTotal;

            return recipe;
        }

        //Actualiza costo de subreceta y regresa la subreceta actualizada
        public SubRecipe UpdateSubRecipeCosts(SubRecipe subRecipe) 
        {
            double costoTotal = 0;

            if (subRecipe.ingredientes.Count > 0)
            {
                costoTotal = CalculateCost(subRecipe.ingredientes);
            }

            subRecipe.costoTotal = costoTotal;
            subRecipe.costoSubrecetaUnidad = costoTotal / subRecipe.unidadesProducidas;

            return subRecipe;
        }

        public void RemoveSubRecipes(SubRecipe subRecipe)
        {

            //Elimino subrecetas de subrecetas
            var subRecipesWithSubRecipes = _subRecipeService.GetSubRecipiesWithSubRecipe(subRecipe.Id, subRecipe.idProyecto);
            var length = subRecipesWithSubRecipes.Count;

            if(length > 0)
            {
                for (int i = 0; i < length; i++)
                {
                    var ingLength = subRecipesWithSubRecipes[i].ingredientes.Count;
                    for (int z = 0; z < ingLength; z++)
                    {
                        if (subRecipesWithSubRecipes[i].ingredientes[z].idIngrediente == subRecipe.Id && subRecipesWithSubRecipes[i].ingredientes[z].tipoReceta == 2)
                        {
                            subRecipesWithSubRecipes[i].ingredientes.RemoveAt(z);
                            _subRecipeService.Update(subRecipesWithSubRecipes[i].Id, subRecipesWithSubRecipes[i]);
                        }
                    }

                    var newSubRecipe = UpdateSubRecipeCosts(subRecipesWithSubRecipes[i]);
                    _subRecipeService.Update(subRecipesWithSubRecipes[i].Id, newSubRecipe);
                }
            }

            //Elimino subrecetas de recetas
            var recipesWithSubRecipe = _recipeService.GetRecipesWithSubRecipe(subRecipe.Id, subRecipe.idProyecto);
            var lengthR = recipesWithSubRecipe.Count;

            if(lengthR > 0)
            {
                for (int i = 0; i < lengthR; i++)
                {
                    var ingLength = recipesWithSubRecipe[i].ingredientes.Count;
                    for (int z = 0; z < ingLength; z++)
                    {
                        if (recipesWithSubRecipe[i].ingredientes[z].idIngrediente == subRecipe.Id && recipesWithSubRecipe[i].ingredientes[z].tipoReceta == 2)
                        {
                            recipesWithSubRecipe[i].ingredientes.RemoveAt(z);
                            _recipeService.Update(recipesWithSubRecipe[i].Id, recipesWithSubRecipe[i]);
                        }
                    }

                    var newRecipe = UpdateRecipeCosts(recipesWithSubRecipe[i]);
                    _recipeService.Update(recipesWithSubRecipe[i].Id, newRecipe);
                }
            }

        }

        public void RemoveIngredients(Ingredient ingredient)
        {
            //Elimino ingrediente de recetas
            var recipesWithIng = _recipeService.GetIngredientInRecipe(ingredient.Id, ingredient.idProyecto);
            var length = recipesWithIng.Count;

            if(length > 0)
            {
                for (int i = 0; i < length; i++)
                {
                    var ingLength = recipesWithIng[i].ingredientes.Count;
                    for (int z = 0; z < ingLength; z++)
                    {
                        if (recipesWithIng[i].ingredientes[z].idIngrediente == ingredient.Id && recipesWithIng[i].ingredientes[z].tipoReceta == 5)
                        {
                            recipesWithIng[i].ingredientes.RemoveAt(z);
                            _recipeService.Update(recipesWithIng[i].Id, recipesWithIng[i]);
                        }
                    }

                    var newRecipe = UpdateRecipeCosts(recipesWithIng[i]);
                    _recipeService.Update(recipesWithIng[i].Id, newRecipe);
                }
            }

            //Elimino ingrediente de subRecetas
            var subRecipesWithIngredient = _subRecipeService.GetIngredientInSubRecipe(ingredient.Id, ingredient.idProyecto);
            var lengthI = subRecipesWithIngredient.Count;

            if (lengthI > 0)
            {
                for (int i = 0; i < lengthI; i++)
                {
                    var ingLength = subRecipesWithIngredient[i].ingredientes.Count;
                    for (int z = 0; z < ingLength; z++)
                    {
                        if (subRecipesWithIngredient[i].ingredientes[z].idIngrediente == ingredient.Id && subRecipesWithIngredient[i].ingredientes[z].tipoReceta == 5)
                        {
                            subRecipesWithIngredient[i].ingredientes.RemoveAt(z);
                            _subRecipeService.Update(subRecipesWithIngredient[i].Id, subRecipesWithIngredient[i]);
                        }
                    }

                    var newSubRecipe = UpdateSubRecipeCosts(subRecipesWithIngredient[i]);
                    _subRecipeService.Update(subRecipesWithIngredient[i].Id, newSubRecipe);
                }
            }
        }

        //Actualiza receta cuando cambia ingrediente
        public void UpdateRecipesCosts(Ingredient ingredient)
        {
            var recipeWithIngredient = _recipeService.GetIngredientInRecipe(ingredient.Id, ingredient.idProyecto);
            var lengthR = recipeWithIngredient.Count;

            if(lengthR > 0)
            {
                for(int i = 0; i < lengthR; i++)
                {
                    var newRecipe = UpdateRecipeCosts(recipeWithIngredient[i]);
                    _recipeService.Update(recipeWithIngredient[i].Id, newRecipe);
                }
            }
        }

        //Actualiza sub receta cuando cambia ingrediente
        public void UpdateSubRecipesCosts(Ingredient ingredient)
        {
            var subRecipeWithIngredient = _subRecipeService.GetIngredientInSubRecipe(ingredient.Id, ingredient.idProyecto);
            var length = subRecipeWithIngredient.Count;

            if (length > 0)
            {
                for(int i = 0; i < length; i++)
                {
                    var newSubRecipe = UpdateSubRecipeCosts(subRecipeWithIngredient[i]);
                    _subRecipeService.Update(subRecipeWithIngredient[i].Id, newSubRecipe);
                }
            }
        }

        //Actualiza receta cuando cambia subreceta
        public void UpdateRecipeCostWhenSubRecipe(SubRecipe subRecipe)
        {
            var recipeWithSubRecipe = _recipeService.GetRecipesWithSubRecipe(subRecipe.Id, subRecipe.idProyecto);
            var length = recipeWithSubRecipe.Count;

            if(length > 0)
            {
                for (int i = 0; i < length; i++)
                {
                    var newRecipe = UpdateRecipeCosts(recipeWithSubRecipe[i]);
                    _recipeService.Update(recipeWithSubRecipe[i].Id, newRecipe);
                }
            }
        }

        //Actualiza Sub receta cuando cambia subreceta
        public void UpdateSubRecipeCostWhenSubRecipe(SubRecipe subRecipe)
        {
            var subRecipeWithSubRecipe = _subRecipeService.GetSubRecipiesWithSubRecipe(subRecipe.Id, subRecipe.idProyecto);
            var length = subRecipeWithSubRecipe.Count;

            if(length > 0)
            {
                for(int i = 0; i < length; i++)
                {
                    var newSubRecipe = UpdateSubRecipeCosts(subRecipeWithSubRecipe[i]);
                    _subRecipeService.Update(subRecipeWithSubRecipe[i].Id, newSubRecipe);
                }
            }
        }

        //Actualiza el costo de 1 costo
        public void UpdateServiceCostCost(ServiceCost serviceCost)
        {
            var consumables = _serviceCost.GetConsumablesFromServiceCost(serviceCost.Id);
            double cost = 0;
            if (consumables.Count > 0)
            {
                for (int i = 0; i < consumables.Count; i++)
                {
                    var consumable = _consumableService.Get(consumables[i].idInsumo);
                    cost = cost + consumable.costo;
                }
            }

            serviceCost.costo = cost;
            _serviceCost.Update(serviceCost.Id, serviceCost);
        }

        //Elimina un costo de servicio y actualiza el costo de recetas
        public void removeCostServiceFromRecipes(List<Recipe> recipes)
        {
            var length = recipes.Count;

            for (int i = 0; i < length; i++)
            {
                recipes[i].costoServicio = 0;
                recipes[i] = UpdateRecipeCosts(recipes[i]);
                _recipeService.Update(recipes[i].Id, recipes[i]);
            }
        }

        //Actualiza el costo del costo de servicio
        public void updateCostFromServiceCost(ServiceCost service)
        {
            var consumables = _serviceCost.GetConsumablesFromServiceCost(service.Id);
            service.costo = 0;
            double cost = 0;
            if (consumables.Count > 0)
            {
                for(int i = 0; i < consumables.Count; i++)
                {
                    var consumable = _consumableService.Get(consumables[i].idInsumo);
                    cost = cost + consumable.costo;
                }

                service.costo = cost;
            }
            _serviceCost.Update(service.Id, service);
        }

        //Actualiza el costo del costo de servicios cuando cambia un insumo y actualiza los costos de las recetas afectadas
        public void updateCostServiceAndRecipeWhenConsumable(int idConsumable)
        {
            var consInService = _serviceCost.GetServiceCostWithConsumable(idConsumable);
            var length = consInService.Count;

            if (length > 0)
            {
                for(int i = 0; i < length; i++)
                {
                    var serviceCost = _serviceCost.Get(consInService[i].idCostoServicio);
                    if(serviceCost != null)
                    {
                        updateCostFromServiceCost(serviceCost);
                        var recipesWithServiceCost = _recipeService.GetRecipesWithServiceCost(serviceCost.Id);
                        for(int z = 0; z < recipesWithServiceCost.Count; z++)
                        {
                            var newRecipe = UpdateRecipeCosts(recipesWithServiceCost[i]);
                            _recipeService.Update(newRecipe.Id, newRecipe);
                        }
                    }
                }
            }
        }

        //Elimina la relacion entre costo de servicio e insumo eliminado y actualiza costos de costos de servicio y recetas
        public void removeConsumableFromServiceCostAndRecipes(int idConsumable)
        {
            var consInService = _serviceCost.GetServiceCostWithConsumable(idConsumable);
            
            if(consInService.Count > 0)
            {
                for(int i = 0; i < consInService.Count; i++)
                {
                    var serviceCost = _serviceCost.Get(consInService[i].Id);
                    _serviceCost.RemoveConsumableFromServiceCost(idConsumable, serviceCost.Id);

                    if(serviceCost != null)
                    {
                        updateCostFromServiceCost(serviceCost);
                        var recipesWithServiceCost = _recipeService.GetRecipesWithServiceCost(serviceCost.Id);
                        for (int z = 0; z < recipesWithServiceCost.Count; z++)
                        {
                            var newRecipe = UpdateRecipeCosts(recipesWithServiceCost[i]);
                            _recipeService.Update(newRecipe.Id, newRecipe);
                        }
                    }
                }
            }
        }

        //Obtiene una linea de menu por la receta especificada
        public RecipeForMenu getRowForMenu(int recipeId)
        {
            var recipe = _recipeService.Get(recipeId);
            RecipeForMenu recipeForMenu = new RecipeForMenu();
            var fixedCosts = _fixedCostService.GetByProject(recipe.idProyecto);
            var fixedCostTotal = 0.0;
            var profits = _profitService.GetByProject(recipe.idProyecto);
            var totalProfits = 0.0;

            if (fixedCosts.Count > 0)
            {
                for (int z = 0; z < fixedCosts.Count; z++)
                {
                    fixedCostTotal = fixedCostTotal + fixedCosts[z].costo;
                }
            }

            if (profits.Count > 0)
            {
                for (int i = 0; i < profits.Count; i++)
                {
                    totalProfits = totalProfits + profits[i].ganancia;
                }
            }

            var profitsMean = totalProfits / profits.Count;
            var percentage = (fixedCostTotal * 100) / profitsMean;
            var unitFixedCost = percentage * 0.01;

            recipeForMenu.idReceta = recipe.Id;
            recipeForMenu.tipoReceta = recipe.tipoReceta;
            recipeForMenu.nombre = recipe.nombre;
            recipeForMenu.precio = recipe.precio;
            recipeForMenu.costoVariable = Math.Round(recipe.costoTotal, 3);
            recipeForMenu.costoFijo = Math.Round(unitFixedCost * recipeForMenu.costoVariable, 3);
            recipeForMenu.costoTotal = Math.Round(recipeForMenu.costoVariable + recipeForMenu.costoFijo, 3);
            recipeForMenu.utilidad = Math.Round(recipeForMenu.precio - recipeForMenu.costoTotal, 3);
            recipeForMenu.porcentajeUtilidad = Math.Round((recipeForMenu.utilidad / recipe.precio) * 100);

            return recipeForMenu;
        }

        //Obtiene el Menu completo de un proyecto
        public List<RecipeForMenu> getMenu(int idProyecto)
        {
            List<RecipeForMenu> menu = new List<RecipeForMenu>();
            var recipes = _recipeService.GetByProject(idProyecto);
            var profits = _profitService.GetByProject(idProyecto);
            var fixedCosts = _fixedCostService.GetByProject(idProyecto);

            var fixedCostTotal = 0.0;
            var totalProfits = 0.0;

            if (fixedCosts.Count > 0)
            {
                for (int z = 0; z < fixedCosts.Count; z++)
                {
                    fixedCostTotal = fixedCostTotal + fixedCosts[z].costo;
                }
            }

            if (profits.Count > 0)
            {
                for (int i = 0; i < profits.Count; i++)
                {
                    totalProfits = totalProfits + profits[i].ganancia;
                }
            }

            var profitsMean = totalProfits / profits.Count;
            var percentage = (fixedCostTotal * 100) / profitsMean;
            var unitFixedCost = percentage * 0.01;

            for (int i = 0; i < recipes.Count; i++)
            {
                RecipeForMenu recipe = new RecipeForMenu();
                double precio = 100;
                if (recipes[i].precio != 0)
                {
                    precio = recipes[i].precio;
                }

                recipe.idReceta = recipes[i].Id;
                recipe.nombre = recipes[i].nombre;
                recipe.tipoReceta = recipes[i].tipoReceta;
                recipe.precio = precio;
                recipe.costoVariable = Math.Round(recipes[i].costoTotal, 3);
                recipe.costoFijo = Math.Round(unitFixedCost * recipe.costoVariable, 3);
                recipe.costoTotal = Math.Round(recipe.costoVariable + recipe.costoFijo, 3);
                recipe.utilidad = Math.Round(recipe.precio - recipe.costoTotal, 3);
                recipe.porcentajeUtilidad = Math.Round((recipe.utilidad / recipe.precio) * 100);

                menu.Add(recipe);
            }

            return menu;
        }

        public double GetSumFixedCosts(int projectId)
        {
            var fixedCosts = _fixedCostService.GetByProject(projectId);
            var fixedCostTotal = 0.0;

            if (fixedCosts.Count > 0)
            {
                for (int z = 0; z < fixedCosts.Count; z++)
                {
                    fixedCostTotal = fixedCostTotal + fixedCosts[z].costo;
                }
            }

            return fixedCostTotal;
        }

        public void arreglarCostosIngredientes()
        {
            var ingredientes = _ingredientService.Get();
            for(int i=0; i < ingredientes.Count; i++)
            {
                var ingredient = ingredientes[i];
                double primeraParte = ingredient.precioPresentacion / ingredient.presentacion;
                double parteIntermedia = Convert.ToDouble(ingredient.merma) / 100;
                double segundaParte = 1 - parteIntermedia;
                double terceraParte = primeraParte / segundaParte;
                ingredient.precioUnitario = terceraParte;
                _ingredientService.Update(ingredient.Id, ingredient);
                UpdateRecipesCosts(ingredient);
                UpdateSubRecipesCosts(ingredient);
            }
        }

        public string getImageOfProject(int id)
        {
            var filePath = "/var/img/projectimg_" + id + ".png";
            string base64;
            
            if (File.Exists(filePath))
            {
                var ImgPath = baseUrl+"projectimg_" + id + ".png";

                base64 = ImgPath;
            }
            else
            {
                var ImgPath = baseUrl + "defaultProject.png";
                base64 = ImgPath;
            }

            return base64;
        }

        public void saveImage(string imgName, string base64)
        {
            var filePath = "/var/img/" + imgName + ".png";
            File.WriteAllBytes(filePath, Convert.FromBase64String(base64));
        }

        public string getImage(int type, int id)
        {
            string imgPath;
            string filePath;
            switch (type)
            {
                case 1:
                        filePath = "/var/img/recipeimg_" + id + ".png";
                        if (File.Exists(filePath))
                        {
                            imgPath = baseUrl +"recipeimg_" + id + ".png";
                        }
                        else
                        {
                            imgPath = baseUrl + "defaultRecipe.png";
                        }

                    break;
                case 2:
                        filePath = "/var/img/subrecipeimg_" + id + ".png";
                        if (File.Exists(filePath))
                        {
                            imgPath = baseUrl + "subrecipeimg_" + id + ".png";
                        }
                        else
                        {
                            imgPath = baseUrl + "defaultSubRecipe.png";
                        }
                    break;
                default:
                        filePath = "/var/img/projectimg_" + id + ".png";
                        if (File.Exists(filePath))
                        {
                            imgPath = baseUrl + "projectimg_" + id + ".png";
                        }
                        else
                        {
                            imgPath = baseUrl + "defaultProject.png";
                        }
                    break;
            }

            return imgPath;
        }

        public void removeCategoryFromRecipes(int categoryId)
        {
            var recipes = _recipeService.GetRecipesByCategory(categoryId);

            if(recipes.Count > 0)
            {
                for(int i=0; i < recipes.Count; i++)
                {
                    recipes[i].categoria = 0;
                    _recipeService.Update(recipes[i].Id, recipes[i]);
                }
            }
        }
        /*
        public void UpdateIngPos(int idRecipe, int idIngredient, int recipeType, int pos)
        {
            var recipe = _recipeService.Get(idRecipe);

            var ingredients = recipe.ingredientes;

            Boolean found = false;

            for (int i = 0; i < ingredients.Count; i++)
            {
                if (ingredients[i].idIngrediente == idIngredient && ingredients[i].tipoReceta == recipeType && found == false)
                {
                    found = true;

                    if (ingredients[i].posicion > pos)
                    {

                    }
                    else
                    {

                    }
                }
            }
        }*/
    }
}
