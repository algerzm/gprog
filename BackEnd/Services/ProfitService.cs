﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using GProGApi.Models;

namespace GProGApi.Services
{
    public class ProfitService
    {
        public readonly IMongoCollection<Profit> _profit;

        public ProfitService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _profit = database.GetCollection<Profit>(settings.ProfitsCollectionName);
        }

        public List<Profit> Get() =>
            _profit.Find<Profit>(Profit => true).ToList();

        public Profit Get(int id) =>
            _profit.Find<Profit>(Profit => Profit.Id == id).FirstOrDefault();

        public List<Profit> GetByProject(int project) =>
            _profit.Find<Profit>(Profit => Profit.idProyecto == project).ToList();

        public List<Profit> GetByDate(string date, int project) =>
            _profit.Find<Profit>(Profit => Profit.fecha == date && Profit.idProyecto == project).ToList();

        public Profit Create(Profit newProfit)
        {
            int id = 1;
            try
            {
                var max = _profit.Find(Profit => true).SortByDescending(Profit => Profit.Id).FirstOrDefault();
                id = max.Id + 1;
                newProfit.Id = id;
            }
            catch (Exception)
            {
                newProfit.Id = id;
            }

            _profit.InsertOne(newProfit);
            return newProfit;
        }

        public void Update(int id, Profit newProfit) =>
            _profit.ReplaceOne(Profit => Profit.Id == id, newProfit);

        public void Remove(int id) =>
            _profit.DeleteOne(Profit => Profit.Id == id);

        public void RemoveAllFromProject(int id) =>
            _profit.DeleteMany(Profit => Profit.idProyecto == id);
    }
}
