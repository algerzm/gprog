﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using MongoDB.Driver;

namespace GProGApi.Services
{
    public class SupplierService
    {
        private readonly IMongoCollection<Supplier> _suppliers;

        public SupplierService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _suppliers = database.GetCollection<Supplier>(settings.SuppliersCollectionName);
        }

        public List<Supplier> Get() =>
            _suppliers.Find(Supplier => true).ToList();

        public Supplier Get(int id) =>
            _suppliers.Find<Supplier>(Supplier => Supplier.Id == id).FirstOrDefault();

        public List<Supplier> GetByProject(int project) =>
            _suppliers.Find<Supplier>(Supplier => Supplier.idProyecto == project).ToList();

        public List<Supplier> GetByName(string name, int project) {
             return _suppliers.FindSync<Supplier>(Supplier => Supplier.nombre.StartsWith(name) && (Supplier.idProyecto == project)).ToList();
        }

        public Supplier Create(Supplier proveedor)
        {
            int id = 1;
            try
            {
                var max = _suppliers.Find(Supplier => true).SortByDescending(Supplier => Supplier.Id).FirstOrDefault();
                id = max.Id + 1;
                proveedor.Id = id;
            }
            catch (Exception)
            {
                proveedor.Id = id;
            }
            

            _suppliers.InsertOne(proveedor);
            return proveedor;
        }

        public void Update(int id, Supplier newSupplier) =>
            _suppliers.ReplaceOne(Supplier => Supplier.Id == id, newSupplier);

        public void Remove(Supplier rmSupplier) =>
            _suppliers.DeleteOne(Supplier => Supplier.Id == rmSupplier.Id);

        public void Remove(int id) =>
            _suppliers.DeleteOne(Supplier => Supplier.Id == id);

        public void RemoveSuppliersFromProject(int projectId) =>
            _suppliers.DeleteMany(Supplier => Supplier.idProyecto == projectId);

    }
}
