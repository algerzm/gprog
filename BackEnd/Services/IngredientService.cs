﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using MongoDB.Driver;

namespace GProGApi.Services
{
    public class IngredientService
    {
        private readonly IMongoCollection<Ingredient> _ingredients;

        public IngredientService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _ingredients = database.GetCollection<Ingredient>(settings.IngredientsCollectionName);
        }

        public List<Ingredient> Get() =>
            _ingredients.Find(Ingredient => true).ToList();

        public Ingredient Get(int id) =>
            _ingredients.Find<Ingredient>(Ingredient => Ingredient.Id == id).FirstOrDefault();

        public List<Ingredient> GetByProjectId(int projectId) =>
            _ingredients.FindSync<Ingredient>(Ingredient => Ingredient.idProyecto == projectId).ToList();

        public List<Ingredient> GetByName(string name, int project)
        {
            return _ingredients.FindSync<Ingredient>(Ingredient => Ingredient.nombre.StartsWith(name) && Ingredient.idProyecto == project).ToList();
        }

        public Ingredient Create(Ingredient ingrediente)
        {
            var max = _ingredients.Find(Ingredient => true).SortByDescending(Ingredient => Ingredient.Id).FirstOrDefault();
            int id = max.Id + 1;
            ingrediente.Id = id;

            _ingredients.InsertOne(ingrediente);
            return ingrediente;
        }

        public void Update(int id, Ingredient ingredientIn) =>
            _ingredients.ReplaceOne(Ingredient => Ingredient.Id == id, ingredientIn);

        public void Remove(Ingredient ingredientIn) =>
            _ingredients.DeleteOne(Ingredient => Ingredient.Id == ingredientIn.Id);

        public void Remove(int id) =>
            _ingredients.DeleteOne(Ingredient => Ingredient.Id == id);

        public void RemoveIngredientsFromProject(int projectId) =>
            _ingredients.DeleteMany(Ingredient => Ingredient.idProyecto == projectId);

    }
}
