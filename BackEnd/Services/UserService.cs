﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using MongoDB.Driver;

namespace GProGApi.Services
{
    public class UserService
    {
        private readonly IMongoCollection<Users> _usuarios;

        public UserService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _usuarios = database.GetCollection<Users>(settings.UsersCollectionName);
        }

        public List<Users> Get() =>
            _usuarios.Find(Users => true).ToList();

        public Users Get(int id) =>
            _usuarios.Find<Users>(Users => Users.Id == id).FirstOrDefault();

        public List<Users> SearchByEmail(string correo) =>
            _usuarios.FindSync<Users>(Users => Users.correo.StartsWith(correo)).ToList();

        public Users GetByEmail(string correo) =>
            _usuarios.FindSync<Users>(Users => Users.correo == correo).FirstOrDefault();

        public Users Create(Users usuario)
        {
            var max = _usuarios.Find(Users => true).SortByDescending(Users => Users.Id).FirstOrDefault();
            int id = max.Id + 1;
            usuario.Id = id;

            _usuarios.InsertOne(usuario);
            return usuario;
        }

        public void Update(int id, Users usuarioEn) =>
            _usuarios.ReplaceOne(Users => Users.Id == id, usuarioEn);

        public void Remove(Users usuarioEn) =>
            _usuarios.DeleteOne(Users => Users.Id == usuarioEn.Id);

        public void Remove(int id) =>
            _usuarios.DeleteOne(Users => Users.Id == id);
    }
}
