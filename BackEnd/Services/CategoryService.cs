﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using GProGApi.Models;

namespace GProGApi.Services
{
    public class CategoryService
    {
        private readonly IMongoCollection<Category> _category;

        public CategoryService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _category = database.GetCollection<Category>(settings.CategoriesCollectionName);
        }

        public List<Category> Get() =>
            _category.Find(Category => true).ToList();

        public Category Get(int id) =>
            _category.Find<Category>(Category => Category.Id == id).FirstOrDefault();

        public Category Create(Category newCategory)
        {
            int id = 1;
            try
            {
                var max = _category.Find(Category => true).SortByDescending(Category => Category.Id).FirstOrDefault();
                id = max.Id + 1;
                newCategory.Id = id;
            }
            catch (Exception)
            {
                newCategory.Id = id;
            }

            _category.InsertOne(newCategory);
            return newCategory;
        }

        public void Update(int id, Category newCategory) =>
            _category.ReplaceOne(Category => Category.Id == id, newCategory);

        public void Remove(int id) =>
            _category.DeleteOne(Category => Category.Id == id);
    }
}
