﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using GProGApi.Models;

namespace GProGApi.Services
{
    public class ServiceCostService
    {
        private readonly IMongoCollection<ServiceCost> _serviceCost;
        private readonly IMongoCollection<ConsumableInServiceCost> _consumableService;

        public ServiceCostService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _serviceCost = database.GetCollection<ServiceCost>(settings.ServicesCostsCollectionName);
            _consumableService = database.GetCollection<ConsumableInServiceCost>(settings.ConsumableServicesCostsCollectionName);
        }

        public List<ServiceCost> Get() =>
            _serviceCost.Find(ServiceCost => true).ToList();

        public List<ConsumableInServiceCost> GetConsumablesFromServiceCost(int idService) =>
            _consumableService.Find(ConsumableInServiceCost => ConsumableInServiceCost.idCostoServicio == idService).ToList();

        public ServiceCost Get(int id) =>
            _serviceCost.Find<ServiceCost>(ServiceCost => ServiceCost.Id == id).FirstOrDefault();

        public List<ServiceCost> GetByName(string name, int project) =>
            _serviceCost.Find<ServiceCost>(ServiceCost => ServiceCost.nombre.StartsWith(name) && ServiceCost.idProyecto == project).ToList();

        public List<ServiceCost> GetByProject(int project) =>
            _serviceCost.Find<ServiceCost>(ServiceCost => ServiceCost.idProyecto == project).ToList();

        public ConsumableInServiceCost GetConsumable(int idCons, int idService) =>
            _consumableService.Find<ConsumableInServiceCost>(ConsumableInServiceCost => ConsumableInServiceCost.idCostoServicio == idService && ConsumableInServiceCost.idInsumo == idCons).FirstOrDefault();

        public List<ConsumableInServiceCost> GetServiceCostWithConsumable(int idConsumable) =>
            _consumableService.Find<ConsumableInServiceCost>(ConsumableInServiceCost => ConsumableInServiceCost.idInsumo == idConsumable).ToList();

        public ServiceCost Create (ServiceCost newServiceCost)
        {
            int id = 1;
            try
            {
                var max = _serviceCost.Find(ServiceCost => true).SortByDescending(ServiceCost => ServiceCost.Id).FirstOrDefault();
                id = max.Id + 1;
                newServiceCost.Id = id;
            }
            catch (Exception)
            {
                newServiceCost.Id = id;
            }

            _serviceCost.InsertOne(newServiceCost);
            return newServiceCost;
        }

        public ConsumableInServiceCost addToServiceCost(ConsumableInServiceCost newConsumableToServiceCost)
        {
            int id = 1;
            try
            {
                var max = _consumableService.Find(ConsumableInServiceCost => true).SortByDescending(ConsumableInServiceCost => ConsumableInServiceCost.Id).FirstOrDefault();
                id = max.Id + 1;
                newConsumableToServiceCost.Id = id;
            }
            catch (Exception)
            {
                newConsumableToServiceCost.Id = id;
            }

            _consumableService.InsertOne(newConsumableToServiceCost);
            return newConsumableToServiceCost;
        }

        public void Update(int id, ServiceCost newServiceCost) =>
            _serviceCost.ReplaceOne(ServiceCost => ServiceCost.Id == id, newServiceCost);

        public void Remove(int id) =>
            _serviceCost.DeleteOne(ServiceCost => ServiceCost.Id == id);

        public void RemoveServiceCostsOfProject(int projectId)
        {
            var serviceCosts = GetByProject(projectId);
            if (serviceCosts.Count > 0)
            {
                for(int i = 0; i < serviceCosts.Count; i++)
                {
                    _serviceCost.DeleteOne(ServiceCost => ServiceCost.Id == serviceCosts[i].Id);
                    _consumableService.DeleteMany(Consumable => Consumable.idCostoServicio == serviceCosts[i].Id);
                }
            }
        }

        public void RemoveConsumableFromServiceCost(int consumable, int serviceCost) =>
            _consumableService.DeleteOne(ConsumableInServiceCost => ConsumableInServiceCost.idInsumo == consumable && ConsumableInServiceCost.idCostoServicio == serviceCost);

        public void RemoveAllConsumablesFromServiceCost(int serviceCost) =>
            _consumableService.DeleteMany(ConsumableInServiceCost => ConsumableInServiceCost.idCostoServicio == serviceCost);
    }
}
