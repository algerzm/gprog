﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using MongoDB.Driver;

namespace GProGApi.Services
{
    public class ProjectService
    {
        private readonly IMongoCollection<Project> _projects;
        private readonly IMongoCollection<UserToProject> _userToProject;

        public ProjectService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _projects = database.GetCollection<Project>(settings.ProjectsCollectionName);
            _userToProject = database.GetCollection<UserToProject>(settings.UsuariosProyectosCollectionName);
        }

        public List<Project> Get() =>
            _projects.Find(Project => true).ToList();

        public Project Get(int id) =>
            _projects.Find<Project>(Project => Project.Id == id).FirstOrDefault();

        public List<Project> GetByName(string name) =>
            _projects.FindSync<Project>(Project => Project.nombre.StartsWith(name)).ToList();

        public List<UserToProject> GetProjectsByUser(int id) =>
            _userToProject.FindSync<UserToProject>(UserToProject => UserToProject.idUsuario == id).ToList();

        public List<UserToProject> GetUsersFromProject(int projectId) =>
            _userToProject.FindSync<UserToProject>(UserToProject => UserToProject.idProyecto == projectId).ToList();

        public UserToProject GetUserInProject(int userId, int projectId) =>
            _userToProject.FindSync<UserToProject>(UserToProject => UserToProject.idUsuario == userId && UserToProject.idProyecto == projectId).FirstOrDefault();

        public Project Create(Project newProject)
        {
            var max = _projects.Find(Project => true).SortByDescending(Project => Project.Id).FirstOrDefault();
            int id = max.Id + 1;
            newProject.Id = id;

            _projects.InsertOne(newProject);
            return newProject;
        }

        public void AddUserToProject(int projectId, int userId)
        {
            UserToProject u2p = new UserToProject();
            var max = _userToProject.Find(UserToProject => true).SortByDescending(UserToProject => UserToProject.Id).FirstOrDefault();
            int id = max.Id + 1;
            u2p.Id = id;
            u2p.idProyecto = projectId;
            u2p.idUsuario = userId;

            _userToProject.InsertOne(u2p);
        }

        public void Update(int id, Project newProject) =>
            _projects.ReplaceOne(Project => Project.Id == id, newProject);

        public void Remove(Project rmProject) =>
            _projects.DeleteOne(Project => Project.Id == rmProject.Id);

        public void Remove(int id) =>
            _projects.DeleteOne(Project => Project.Id == id);

        public void RemoveOneUserFromProject(UserToProject userToProjectOut) =>
            _userToProject.DeleteOne(UserToProject => UserToProject.idProyecto == userToProjectOut.idProyecto && UserToProject.idUsuario == userToProjectOut.idUsuario);

        public void RemoveUserFromProject(int userId)=>
            _userToProject.DeleteMany(UserToProject => UserToProject.idUsuario == userId);
        

        public void RemoveProjects(int projectId) =>
            _userToProject.DeleteMany(UserToProject => UserToProject.idProyecto == projectId);

    }
}
