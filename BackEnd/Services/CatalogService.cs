﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using MongoDB.Driver;

namespace GProGApi.Services
{
    public class CatalogService
    {
        private readonly IMongoCollection<TypesOfRecipe> _type;
        private readonly IMongoCollection<MeasureUnits> _units;
        private readonly IMongoCollection<Role> _role; 

        public CatalogService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _type = database.GetCollection<TypesOfRecipe>(settings.TypesOfRecipesCollectionName);
            _units = database.GetCollection<MeasureUnits>(settings.MeasureUnitsCollectionName);
            _role = database.GetCollection<Role>(settings.RolesCollectionName);
        }

        public List<TypesOfRecipe> GetTypes() =>
            _type.Find<TypesOfRecipe>(TypesOfRecipe => true).ToList();

        public TypesOfRecipe GetTypeId(int id) =>
            _type.Find<TypesOfRecipe>(TypesOfRecipe => TypesOfRecipe.Id == id).FirstOrDefault();

        public List<MeasureUnits> GetMeasureUnits() =>
            _units.Find<MeasureUnits>(MeasureUnits => true).ToList();

        public MeasureUnits GetUnitId(int id) =>
            _units.Find<MeasureUnits>(MeasureUnits => MeasureUnits.Id == id).FirstOrDefault();

        public List<Role> GetRoles() =>
            _role.Find<Role>(Role => true).ToList();

        public Role GetRoleId(int id) =>
            _role.Find<Role>(Role => Role.Id == id).FirstOrDefault();
    }
}
