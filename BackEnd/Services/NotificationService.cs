﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using GProGApi.Models;

namespace GProGApi.Services
{
    public class NotificationService
    {
        private readonly IMongoCollection<Notification> _notification;
        private readonly IMongoCollection<NotificationReaded> _controlNotification;

        public NotificationService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _notification = database.GetCollection<Notification>(settings.NotificationsCollectionName);
            _controlNotification = database.GetCollection<NotificationReaded>(settings.NotificationsControlCollectionName);
        }

        public List<Notification> Get() =>
            _notification.Find(Notification => true).ToList();

        public Notification Get(int id) =>
            _notification.Find(Notification => Notification.Id == id).FirstOrDefault();

        public List<NotificationReaded> GetNotificationsReaded() =>
            _controlNotification.Find(NotificationReaded => true).ToList();

        public List<NotificationReaded> GetReadersOfNotification(int id) =>
            _controlNotification.Find<NotificationReaded>(NotificationReaded => NotificationReaded.idNotificacion == id).ToList();

        public List<NotificationReaded> GetNotificationsReadedOfUser(int id) =>
            _controlNotification.Find<NotificationReaded>(NotificationReaded => NotificationReaded.idUsuario == id).ToList();

        public List<Notification> GetNotificationsExceptList(List<int> notifications)
        {
            var filter = Builders<Notification>.Filter.Nin(Notification => Notification.Id, notifications);
            return _notification.Find<Notification>(filter).ToList();
        }

        public NotificationReaded GetNotificationReaded(int user, int notification) =>
            _controlNotification.Find<NotificationReaded>(NotificationReaded => NotificationReaded.idNotificacion == notification && NotificationReaded.idUsuario == user).FirstOrDefault();

        public Notification GetMaxId() =>
            _notification.Find(Notification => true).SortByDescending(Notification => Notification.Id).FirstOrDefault();

        public Notification Create(Notification newNotification)
        {
            int id = 1;
            try
            {
                var max = _notification.Find(Notification => true).SortByDescending(Notification => Notification.Id).FirstOrDefault();
                id = max.Id + 1;
                newNotification.Id = id;
            }
            catch (Exception)
            {
                newNotification.Id = id;
            }

            _notification.InsertOne(newNotification);
            return newNotification;
        }

        public NotificationReaded ReadedNotification(NotificationReaded newRead)
        {
            int id = 1;
            try
            {
                var max = _controlNotification.Find(NotificationReaded => true).SortByDescending(NotificationReaded => NotificationReaded.Id).FirstOrDefault();
                id = max.Id + 1;
                newRead.Id = id;
            }
            catch (Exception)
            {
                newRead.Id = id;
            }

            _controlNotification.InsertOne(newRead);
            return newRead;
        }

        public void RemoveUsersFromReadedNotifications(int idUser)
        {
            _controlNotification.DeleteMany(Notification => Notification.idUsuario == idUser);
        }

        public void RemoveNotificationsOfUser(int idUser) =>
            _notification.DeleteMany(Notification => Notification.idUsuario == idUser);
    }
}
