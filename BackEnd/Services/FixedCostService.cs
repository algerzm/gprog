﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using GProGApi.Models;

namespace GProGApi.Services
{
    public class FixedCostService
    {
        private readonly IMongoCollection<FixedCost> _fixedCosts;

        public FixedCostService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _fixedCosts = database.GetCollection<FixedCost>(settings.FixedCostsCollectionName);
        }

        public List<FixedCost> Get() =>
            _fixedCosts.Find(FixedCost => true).ToList();

        public FixedCost Get(int id) =>
            _fixedCosts.Find<FixedCost>(FixedCost => FixedCost.Id == id).FirstOrDefault();

        public List<FixedCost> GetByName(string concept, int project) {
            return _fixedCosts.FindSync<FixedCost>(FixedCost => FixedCost.concepto.StartsWith(concept) && FixedCost.idProyecto == project).ToList();
        }

        public List<FixedCost> GetByProject(int projectId) =>
            _fixedCosts.Find<FixedCost>(FixedCost => FixedCost.idProyecto == projectId).ToList();

        public FixedCost Create(FixedCost newFixedCost)
        {
            int id = 1;
            try
            {
                var max = _fixedCosts.Find(FixedCost => true).SortByDescending(FixedCost => FixedCost.Id).FirstOrDefault();
                id = max.Id + 1;
                newFixedCost.Id = id;
            }
            catch (Exception)
            {
                newFixedCost.Id = id;
            }
            

            _fixedCosts.InsertOne(newFixedCost);
            return newFixedCost;
        }

        public void Update (int id, FixedCost newFixedCost) =>
            _fixedCosts.ReplaceOne(FixedCost => FixedCost.Id == id, newFixedCost);

        public void Remove(FixedCost rmFixedCost) =>
            _fixedCosts.DeleteOne(FixedCost => FixedCost.Id == rmFixedCost.Id);

        public void Remove(int id) =>
            _fixedCosts.DeleteOne(FixedCost => FixedCost.Id == id);

        public void RemoveFixedCostFromProject(int projectId) =>
            _fixedCosts.DeleteMany(FixedCost => FixedCost.idProyecto == projectId);

    }
}
