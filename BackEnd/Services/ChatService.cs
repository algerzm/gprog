﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using GProGApi.Models;

namespace GProGApi.Services
{
    public class ChatService
    {
        private readonly IMongoCollection<Chat> _chat;

        public ChatService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _chat = database.GetCollection<Chat>(settings.ChatsCollectionName);
        }

        public List<Chat> Get() =>
            _chat.Find<Chat>(Chat => true).ToList();

        public Chat Get(int id) =>
            _chat.Find<Chat>(Chat => Chat.Id == id).FirstOrDefault();

        public Chat GetByProject(int id) =>
            _chat.Find<Chat>(Chat => Chat.idProyecto == id).FirstOrDefault();

        public Chat Create(Chat newChat)
        {
            int id = 1;
            try
            {
                var max = _chat.Find<Chat>(Chat => true).SortByDescending(Chat => Chat.Id).FirstOrDefault();
                var maxId = max.Id + 1;
                id = maxId;
                newChat.Id = id;
            }
            catch (Exception)
            {
                newChat.Id = id;
            }

            _chat.InsertOne(newChat);

            return newChat;
        }

        public void Update(int id, Chat newChat) =>
            _chat.ReplaceOne(Chat => Chat.Id == id, newChat);

        public void Remove(int id) =>
            _chat.DeleteOne(Chat => Chat.Id == id);

        public void RemoveByProject(int id) =>
            _chat.DeleteMany(Chat => Chat.idProyecto == id);
    }
}
