﻿using GProGApi.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GProGApi.Services
{
    public class SubRecipeService
    {
        private readonly IMongoCollection<SubRecipe> _subRecipe;

        public SubRecipeService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _subRecipe = database.GetCollection<SubRecipe>(settings.SubRecipesCollectionName);
        }

        public List<SubRecipe> Get() =>
            _subRecipe.Find(SubRecipe => true).ToList();

        public SubRecipe Get(int id) =>
            _subRecipe.Find<SubRecipe>(SubRecipe => SubRecipe.Id == id).FirstOrDefault();

        public List<SubRecipe> GetByName(string name, int project) =>
            _subRecipe.FindSync<SubRecipe>(SubRecipe => SubRecipe.nombre.StartsWith(name) && SubRecipe.idProyecto == project).ToList();

        public List<SubRecipe> GetByProject(int projectId) =>
            _subRecipe.Find<SubRecipe>(SubRecipe => SubRecipe.idProyecto == projectId).ToList();

        public List<SubRecipe> GetIngredientInSubRecipe(int ingredientId, int projectId)
        {
            var filter = Builders<SubRecipe>.Filter.ElemMatch(z => z.ingredientes, a => a.idIngrediente == ingredientId && a.tipoReceta == 5);
            filter = filter & (Builders<SubRecipe>.Filter.Eq(x => x.idProyecto, projectId));
            //var resultado = _recipes.Aggregate().Match(filter).Project<IngredientInRecipe>(Builders<Recipe>.Projection.Expression<IngredientInRecipe>(z => z.ingredientes.FirstOrDefault(a => a.idIngrediente == ingredientId))).ToList();
            var resultado = _subRecipe.Find(filter).ToList();
            return resultado;
        }

        public List<SubRecipe> GetSubRecipiesWithSubRecipe(int subRecipeId, int projectId)
        {
            var filter = Builders<SubRecipe>.Filter.ElemMatch(z => z.ingredientes, a => a.idIngrediente == subRecipeId && a.tipoReceta == 2);
            filter = filter & (Builders<SubRecipe>.Filter.Eq(x => x.idProyecto, projectId));
            //var resultado = _recipes.Aggregate().Match(filter).Project<IngredientInRecipe>(Builders<Recipe>.Projection.Expression<IngredientInRecipe>(z => z.ingredientes.FirstOrDefault(a => a.idIngrediente == ingredientId))).ToList();
            var resultado = _subRecipe.Find(filter).ToList();
            return resultado;
        }

        public SubRecipe Create(SubRecipe newSubRecipe)
        {
            int id = 1;
            try
            {
                var max = _subRecipe.Find(SubRecipe => true).SortByDescending(SubRecipe => SubRecipe.Id).FirstOrDefault();
                id = max.Id + 1;
                newSubRecipe.Id = id;
            }
            catch (Exception)
            {
                newSubRecipe.Id = id;
            }


            _subRecipe.InsertOne(newSubRecipe);
            return newSubRecipe;
        }

        public void Update(int id, SubRecipe newSubRecipe) =>
            _subRecipe.ReplaceOne(SubRecipe => SubRecipe.Id == id, newSubRecipe);

        public void Remove(int id) =>
            _subRecipe.DeleteOne(SubRecipe => SubRecipe.Id == id);

        public void RemoveSubRecipesFromProject(int id) =>
            _subRecipe.DeleteMany(SubRecipe => SubRecipe.idProyecto == id);
    }
}
