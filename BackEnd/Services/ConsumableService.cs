﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using GProGApi.Models;

namespace GProGApi.Services
{
    public class ConsumableService
    {
        private readonly IMongoCollection<Consumable> _consumable;

        public ConsumableService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _consumable = database.GetCollection<Consumable>(settings.ConsumablesCollectionName);
        }

        public List<Consumable> Get() =>
            _consumable.Find(Consumable => true).ToList();

        public Consumable Get(int id) =>
            _consumable.Find<Consumable>(Consumable => Consumable.Id == id).FirstOrDefault();

        public List<Consumable> GetByName(string name, int project) =>
            _consumable.FindSync<Consumable>(Consumable => Consumable.nombre.StartsWith(name) && Consumable.idProyecto == project).ToList();

        public List<Consumable> GetByProject(int project) =>
            _consumable.Find<Consumable>(Consumable => Consumable.idProyecto == project).ToList();

        public Consumable Create(Consumable newConsumable)
        {
            int id = 1;
            try
            {
                var max = _consumable.Find(Consumable => true).SortByDescending(Consumable => Consumable.Id).FirstOrDefault();
                id = max.Id + 1;
                newConsumable.Id = id;
            }
            catch (Exception)
            {
                newConsumable.Id = id;
            }

            _consumable.InsertOne(newConsumable);
            return newConsumable;
        }

        public void Update(int id, Consumable newConsumable) =>
            _consumable.ReplaceOne(Consumable => Consumable.Id == id, newConsumable);

        public void Remove(int id) =>
            _consumable.DeleteOne(Consumable => Consumable.Id == id);

        public void RemoveConsumablesFromProject(int projectId) =>
            _consumable.DeleteMany(Consumable => Consumable.idProyecto == projectId);


    }
}
