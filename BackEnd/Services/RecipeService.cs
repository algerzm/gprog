﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using GProGApi.Models;
using Microsoft.EntityFrameworkCore;

namespace GProGApi.Services
{
    public class RecipeService
    {
        private readonly IMongoCollection<Recipe> _recipes;

        public RecipeService(IGProGDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _recipes = database.GetCollection<Recipe>(settings.RecipesCollectionName);
        }

        public List<Recipe> Get() =>
            _recipes.Find(Recipe => true).ToList();

        public Recipe Get(int id) =>
            _recipes.Find<Recipe>(Recipe => Recipe.Id == id).FirstOrDefault();

        public List<Recipe> GetByName(string name, int project) =>
            _recipes.FindSync<Recipe>(Recipe => Recipe.nombre.StartsWith(name) && Recipe.idProyecto == project).ToList();

        public List<Recipe> GetByProject(int projectId) =>
            _recipes.Find<Recipe>(Recipe => Recipe.idProyecto == projectId).ToList();

        public List<Recipe> GetRecipesWithServiceCost(int service) =>
            _recipes.Find<Recipe>(Recipe => Recipe.costoServicio == service).ToList();

        public List<Recipe> GetRecipesByCategory(int categoryId) =>
            _recipes.Find<Recipe>(Recipe => Recipe.categoria == categoryId).ToList();

        public List<Recipe> GetIngredientInRecipe(int ingredientId, int projectId)
        {
            var filter = Builders<Recipe>.Filter.ElemMatch(z => z.ingredientes, a => a.idIngrediente == ingredientId && a.tipoReceta == 5);
            filter = filter & (Builders<Recipe>.Filter.Eq(x => x.idProyecto, projectId));
            //var resultado = _recipes.Aggregate().Match(filter).Project<IngredientInRecipe>(Builders<Recipe>.Projection.Expression<IngredientInRecipe>(z => z.ingredientes.FirstOrDefault(a => a.idIngrediente == ingredientId))).ToList();
            var resultado = _recipes.Find(filter).ToList();
            return resultado;
        }

        public List<Recipe> GetRecipesWithSubRecipe(int subRecipeId, int projectId)
        {
            var filter = Builders<Recipe>.Filter.ElemMatch(z => z.ingredientes, a => a.idIngrediente == subRecipeId && a.tipoReceta == 2);
            filter = filter & (Builders<Recipe>.Filter.Eq(x => x.idProyecto, projectId));
            //var resultado = _recipes.Aggregate().Match(filter).Project<IngredientInRecipe>(Builders<Recipe>.Projection.Expression<IngredientInRecipe>(z => z.ingredientes.FirstOrDefault(a => a.idIngrediente == ingredientId))).ToList();
            var resultado = _recipes.Find(filter).ToList();
            return resultado;
        }

        public Recipe Create(Recipe newRecipe)
        {
            int id = 1;
            try
            {
                var max = _recipes.Find(Recipe => true).SortByDescending(Recipe => Recipe.Id).FirstOrDefault();
                id = max.Id + 1;
                newRecipe.Id = id;
            }
            catch(Exception)
            {
                newRecipe.Id = id;
            }
            

            _recipes.InsertOne(newRecipe);
            return newRecipe;
        }

        public void Update(int id, Recipe newRecipe) =>
            _recipes.ReplaceOne(Recipe => Recipe.Id == id, newRecipe);

        public void Remove(int id) =>
            _recipes.DeleteOne(Recipe => Recipe.Id == id);

        public void RemoveRecipesFromProject(int id) =>
            _recipes.DeleteMany(Recipe => Recipe.idProyecto == id);
    }
}
