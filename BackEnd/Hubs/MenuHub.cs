﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using GProGApi.Models;
using GProGApi.Services;


namespace GProGApi.Hubs
{
    public class MenuHub: Hub
    {
        private readonly RecipeService _recipeService;
        private readonly FixedCostService _fixedCostService;
        private readonly ProfitService _profitService;
        private readonly Utilities _utilities;

        public MenuHub(RecipeService recipeService, FixedCostService fixedCostService, ProfitService profitService, Utilities utilities)
        {
            _recipeService = recipeService;
            _fixedCostService = fixedCostService;
            _profitService = profitService;
            _utilities = utilities;
        }

        public async Task UpdateRecipe(int recipeId, double price)
        {
            var recipe = _recipeService.Get(recipeId);
            var response = false;
            var connectionId = Context.ConnectionId;

            RecipeForMenu recipeForMenu = new RecipeForMenu();

            if (recipe != null && price > 0)
            {
                recipe.precio = price;
                try
                {
                    _recipeService.Update(recipeId, recipe);
                    response = true;
                    recipeForMenu = _utilities.getRowForMenu(recipeId);
                }
                catch (Exception)
                {
                    response = false;
                }
            }

            await Clients.Client(connectionId).SendAsync("UpdateMenu", response, recipeForMenu);
        }
    }
}
