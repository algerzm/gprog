﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using GProGApi.Models;
using GProGApi.Services;

namespace GProGApi.Hubs
{
    public class ChatHub: Hub
    {
        private readonly ChatService _chatService;
        private readonly ProjectService _projectService;
        private readonly UserService _userService;
        //private static TimeZoneInfo Pacific_Standard_Time = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");

        public ChatHub(ChatService chatService, ProjectService projectService, UserService userService)
        {
            _chatService = chatService;
            _projectService = projectService;
            _userService = userService;
        }

        public async Task EnviarMensaje(int idUser, int idProject, string newMessage)
        {
            DateTime dateTime_Pacific = DateTime.UtcNow;
            Mensaje message = new Mensaje();
            var project = _projectService.Get(idProject);
            var user = _userService.Get(idUser);
            var chat = _chatService.GetByProject(idProject);

            if (project != null && user != null && chat != null)
            {
                message.idUsuario = idUser;
                message.mensaje = newMessage;
                message.fecha = dateTime_Pacific;

                var oldMessages = chat.mensajes;
                oldMessages.Add(message);
                chat.mensajes = oldMessages;

                _chatService.Update(chat.Id, chat);

                await Clients.Group(idProject.ToString()).SendAsync("ReceiveMessage", message, user);
            }
        }

        public async Task<string> JoinRoom(string idProyecto)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, idProyecto);
            return "Si se hizo la conexion";
        }
    }
}
