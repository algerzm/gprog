﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using GProGApi.Models;
using GProGApi.Services;

namespace GProGApi.Hubs
{
    public class NotificationHub: Hub
    {
        private readonly UserService _userService;
        private readonly NotificationService _notification;

        public NotificationHub(UserService userService, NotificationService notification)
        {
            _userService = userService;
            _notification = notification;
        }

        public async Task SendNotification(string title, string message, int user)
        {
            DateTime date = DateTime.Now;

            Notification not = new Notification();
            not.titulo = title;
            not.mensaje = message;
            not.idUsuario = user;
            not.hora = date;

            _notification.Create(not);
            var max = _notification.GetMaxId();
            var maxId = max.Id;

            await Clients.All.SendAsync("ReceiveNotification", title, message, maxId);
        }
    }
}
