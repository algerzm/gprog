﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3,4")]
    public class SuppliersController
    {
        private readonly SupplierService _supplierService;
        private readonly ProjectService _projectService;

        public SuppliersController(SupplierService supplierService, ProjectService projectService)
        {
            _supplierService = supplierService;
            _projectService = projectService;
        }
        
        [HttpGet]
        public ActionResult<List<Supplier>> Get() =>
            _supplierService.Get();

        [HttpGet("{id}", Name = "GetSupplier")]
        public ActionResult<Supplier> Get(int id)
        {
            var supplier = _supplierService.Get(id);

            if(supplier == null)
            {
                return new NotFoundResult();
            }

            return supplier;
        }

        [HttpGet("fromProject/{id}")]
        public ActionResult<List<Supplier>> GetByProject(int id)
        {
            var project = _projectService.Get(id);
           
            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto: " + id + ", no existe");
            }

            var suppliers = _supplierService.GetByProject(id);

            if(suppliers.Count == 0)
            {
                return new NotFoundObjectResult("Este proyecto no tiene proveedores asignados");
            }

            return new OkObjectResult(suppliers);

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost("{nombre}", Name = "GetSupplierByName")]
        public ActionResult<List<Supplier>> Get(SearchSupplierName supplierName)
        {
            var supplier = _supplierService.GetByName(supplierName.nombre, supplierName.idProyecto);

            if (supplier.Count == 0)
            {
                return new NotFoundResult();
            }

            return supplier;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost]
        public ActionResult<Supplier> Create(Supplier supplier)
        {
            var project = _projectService.Get(supplier.idProyecto);

            if (project == null)
            {
                return new NotFoundResult();
            }

            try
            {
                _supplierService.Create(supplier);
            }
            catch(Exception e)
            {
                return new ConflictResult();
            }

            return new CreatedAtRouteResult("GetSupplier", new { id = supplier.Id.ToString() }, supplier);

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("{id}")]
        public IActionResult Update(int id, Supplier newSupplier)
        {
            var project = _projectService.Get(newSupplier.idProyecto);

            if (project == null)
            {
                return new NotFoundResult();
            }

            var supplier = _supplierService.Get(id);

            if(supplier == null)
            {
                return new NotFoundResult();
            }

            newSupplier.Id = id;
            _supplierService.Update(id, newSupplier);

            return new NoContentResult();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var supplier = _supplierService.Get(id);

            if(supplier == null)
            {
                return new NotFoundResult();
            }

            _supplierService.Remove(id);

            return new NoContentResult();
        }
    }
}
