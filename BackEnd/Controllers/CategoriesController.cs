﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2")]
    public class CategoriesController
    {
        private readonly CategoryService _categoryService;
        private readonly Utilities _utilities;

        public CategoriesController(CategoryService categoryService, Utilities utilities)
        {
            _categoryService = categoryService;
            _utilities = utilities;
        }

        [HttpGet]
        public ActionResult<List<Category>> Get() =>
            _categoryService.Get();

        [HttpGet("{id}")]
        public ActionResult<Category> Get(int id)
        {
            var category = _categoryService.Get(id);

            if(category == null)
            {
                return new NotFoundObjectResult("No existe la categoria: " + id);
            }

            return new OkObjectResult(category);
        }

        [HttpPost(Name = "CreateCategory")]
        public ActionResult<Consumable> Create (Category newCategory)
        {
            var categories = _categoryService.Get();
            var existCategory = categories.Find(category => category.nombre == newCategory.nombre);

            if(existCategory != null)
            {
                return new ConflictObjectResult("La categoria" + newCategory.nombre + ", ya existe en la base de datos");
            }

            try
            {
                _categoryService.Create(newCategory);
            }
            catch (Exception)
            {
                return new ConflictObjectResult("Hubo un problema al crear la categoria");
            }

            return new CreatedAtRouteResult("CreateCategory", new { id = newCategory.Id.ToString() }, newCategory);
        }

        [HttpPut("{id}")]
        public ActionResult<Category> Update(int id, Category newCategory)
        {
            var category = _categoryService.Get(id);

            if(category == null)
            {
                return new NotFoundObjectResult("La categoria: " + id + ", no se encontro en la base de datos");
            }

            newCategory.Id = category.Id;
            _categoryService.Update(id, newCategory);

            return new OkObjectResult(newCategory);
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if(id == 0)
            {
                return new ConflictObjectResult("No esta permitido eliminar esta categoria");
            }

            var category = _categoryService.Get(id);

            if(category == null)
            {
                return new NotFoundObjectResult("La categoria a eliminar no existe");
            }

            _categoryService.Remove(id);
            //Cambio la categoria de las recetas que tenian esta categoria
            _utilities.removeCategoryFromRecipes(id);

            return new OkResult();
        }
    }
}
