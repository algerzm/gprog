﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using System.IO;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class RecipesController
    {
        private readonly RecipeService _recipeService;
        private readonly IngredientService _ingredientService;
        private readonly ProjectService _projectService;
        private readonly Utilities _utilities;
        private readonly ServiceCostService _serviceCostService;
        private readonly FixedCostService _fixedCostService;
        private readonly ProfitService _profitService;
        private readonly CatalogService _catalogService;
        private readonly CategoryService _categoryService;
        private readonly string baseUrl;

        public RecipesController(RecipeService recipeService, IngredientService ingredientService, ProjectService projectService, Utilities utilities, ServiceCostService serviceCostService, FixedCostService fixedCostService, ProfitService profitService, CatalogService catalogService, CategoryService categoryService)        {
            _recipeService = recipeService;
            _ingredientService = ingredientService;
            _projectService = projectService;
            _utilities = utilities;
            _serviceCostService = serviceCostService;
            _fixedCostService = fixedCostService;
            _profitService = profitService;
            _catalogService = catalogService;
            _categoryService = categoryService;

            Globals global = new Globals();
            baseUrl = global.getBaseUrl();
        }

        [HttpGet]
        public ActionResult<List<Recipe>> Get() =>
            _recipeService.Get();

        [HttpGet("{id}")]
        public ActionResult<Recipe> Get(int id)
        {
            var recipe = _recipeService.Get(id);
            
            if (recipe == null)
            {
                return new NotFoundObjectResult("No se encontro la recera: " + id);
            }

            try
            {
                var filePath = "/var/img/recipeimg_" + id + ".png";

                if (File.Exists(filePath))
                {
                    Byte[] bytes = File.ReadAllBytes(filePath);

                    recipe.imagen = Convert.ToBase64String(bytes);
                }
                else
                {
                    Byte[] bytes = File.ReadAllBytes("/var/img/defaultRecipe.png");
                    recipe.imagen = Convert.ToBase64String(bytes);
                }

            }
            catch (Exception e)
            {
                return new ConflictObjectResult("Ocurrio un error al obtener la imagen: " + e.ToString());
            }

            return new OkObjectResult(recipe);
        }

        [HttpGet("fromProject/{projectId}")]
        public ActionResult<List<Recipe>> GetFromProject(int projectId)
        {
            var recipes = _recipeService.GetByProject(projectId);
            var categories = _categoryService.Get();

            for(int i=0; i<recipes.Count; i++)
            {
                try
                {
                    var filePath = "/var/img/recipeimg_" + recipes[i].Id + ".png";

                    if (File.Exists(filePath))
                    {
                        var ImgPath = baseUrl+"recipeimg_" + recipes[i].Id + ".png";

                        recipes[i].imagen = ImgPath;
                    }
                    else
                    {
                        var ImgPath = baseUrl + "default.png";

                        recipes[i].imagen = ImgPath;
                    }

                }
                catch (Exception e)
                {
                    return new ConflictObjectResult("Ocurrio un error al obtener la imagen: " + e.ToString());
                }
            }

            if(recipes.Count == 0)
            {
                return new NotFoundObjectResult("No hay recetas asignadas en este proyecto");
            }

            return new OkObjectResult(recipes);
        }

        [HttpGet("getPicture/{id}")]
        public ActionResult<ProfilePicture> getProfilePicture(int id)
        {
            var recipe = _recipeService.Get(id);

            if (recipe == null)
            {
                return new NotFoundObjectResult("No se encontro la receta: " + id);
            }

            ProfilePicture picture = new ProfilePicture();

            try
            {
                var filePath = "/var/img/recipeimg_" + id + ".png";

                if (File.Exists(filePath))
                {
                    Byte[] bytes = File.ReadAllBytes(filePath);

                    picture.base64 = Convert.ToBase64String(bytes);
                }
                else
                {
                    Byte[] bytes = File.ReadAllBytes("/var/img/defaultRecipe.png");
                    picture.base64 = Convert.ToBase64String(bytes);
                }

            }
            catch (Exception e)
            {
                return new ConflictObjectResult("Ocurrio un error al obtener la imagen: " + e.ToString());
            }

            return new OkObjectResult(picture);
        }

        [HttpGet("ingFromRecipe/{recipeId}")]
        public ActionResult<List<IngredientInRecipe>> GetIngredientsFromRecipe(int recipeId)
        {
            var recipe = _recipeService.Get(recipeId);

            if(recipe == null)
            {
                return new NotFoundObjectResult("No existe la receta");
            }

            if(recipe.ingredientes.Count == 0)
            {
                return new NotFoundObjectResult("No hay ingredientes asignados a esta receta");
            }

            return recipe.ingredientes;
        }

        [HttpGet("ingFromRecipeWithName/{projectId}")]
        public ActionResult<List<RecipeWithNames>> GetIngredientsFromRecipeWithName(int projectId)
        {
            var recipe = _recipeService.GetByProject(projectId);
            List<RecipeWithNames> recipes = new List<RecipeWithNames>();
            var categories = _categoryService.Get();

            for(int x=0; x<recipe.Count; x++)
            {
                List<IngredientInRecipeName> ingsWithName = new List<IngredientInRecipeName>();
                var ingredients = _ingredientService.GetByProjectId(recipe[x].idProyecto);
                var measureUnits = _catalogService.GetMeasureUnits();
                
                for (int i = 0; i < recipe[x].ingredientes.Count; i++)
                {
                    IngredientInRecipeName ing = new IngredientInRecipeName();
                    var otherIng = _utilities.getIngredient(recipe[x].ingredientes[i].idIngrediente, recipe[x].ingredientes[i].tipoReceta);
                    var unit = measureUnits.Find(y => y.Id == recipe[x].ingredientes[i].unidadMedida);
                    ing.idIngrediente = recipe[x].ingredientes[i].idIngrediente;
                    ing.nombre = otherIng.nombre;
                    ing.cantidad = recipe[x].ingredientes[i].cantidad;
                    ing.tipoReceta = recipe[x].ingredientes[i].tipoReceta;
                    ing.unidadMedida = unit.abreviacion;

                    ingsWithName.Add(ing);
                }

                var recipeCategoryId = recipe[x].categoria;
                RecipeWithNames newRecipe = new RecipeWithNames();
                newRecipe.Id = recipe[x].Id;
                newRecipe.nombre = recipe[x].nombre;
                newRecipe.tecnica = recipe[x].tecnica;
                newRecipe.tiempoPreparacion = recipe[x].tiempoPreparacion;
                newRecipe.procedimiento = recipe[x].procedimiento;
                newRecipe.montaje = recipe[x].montaje;
                newRecipe.ingredientes = ingsWithName;
                newRecipe.costoTotal = recipe[x].costoTotal;
                newRecipe.precio = recipe[x].precio;
                newRecipe.tipoReceta = recipe[x].tipoReceta;
                newRecipe.costoServicio = recipe[x].costoServicio;
                newRecipe.idProyecto = recipe[x].idProyecto;
                newRecipe.cuberteria = recipe[x].cuberteria;
                newRecipe.categoria = categories.Find(category => category.Id == recipeCategoryId).nombre;

                try
                {
                    var filePath = "/var/img/recipeimg_" + newRecipe.Id + ".png";

                    if (File.Exists(filePath))
                    {
                        Byte[] bytes = File.ReadAllBytes(filePath);

                        newRecipe.imagen = Convert.ToBase64String(bytes);
                    }
                    else
                    {
                        Byte[] bytes = File.ReadAllBytes("/var/img/defaultRecipe.png");
                        newRecipe.imagen = Convert.ToBase64String(bytes);
                    }

                }
                catch (Exception e)
                {
                    return new ConflictObjectResult("Ocurrio un error al obtener la imagen: " + e.ToString());
                }


                recipes.Add(newRecipe);
            }

            

            return new OkObjectResult(recipes);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost("nombre", Name = "GetRecipeByName")]
        public ActionResult<List<Recipe>> Get(SearchRecipeName searchRecipe)
        {
            var recipes = _recipeService.GetByName(searchRecipe.nombre, searchRecipe.idProyecto);

            if(recipes.Count == 0)
            {
                return new NotFoundObjectResult("No hay coincidencias en el proyecto " + searchRecipe.idProyecto);
            }

            return new OkObjectResult(recipes);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost(Name = "CreateRecipe")]
        public ActionResult<Recipe> Create (Recipe newRecipe)
        {
            var duplicatedRecipe = _recipeService.GetByName(newRecipe.nombre, newRecipe.idProyecto);
            var ingredients = newRecipe.ingredientes;
            var project = _projectService.Get(newRecipe.idProyecto);
            var serviceCost = _serviceCostService.Get(newRecipe.costoServicio);
            var category = _categoryService.Get(newRecipe.categoria);
            
            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto " + newRecipe.idProyecto + ", no existe");
            }

            if(category == null)
            {
                return new NotFoundObjectResult("La categoria seleccionada no existe");
            }

            if (duplicatedRecipe.Count > 0)
            {
                return new ConflictObjectResult("Esta receta ya existe en este proyecto");
            }

            if(newRecipe.costoServicio != 0)
            {
                if (serviceCost.idProyecto != newRecipe.idProyecto)
                {
                    return new ConflictObjectResult("El costo de servicio: " + serviceCost.idProyecto + ", no pertenece a este proyecto");
                }
            }

            if (ingredients.Count > 0)
            {
                var length = ingredients.Count;
                for (int i = 0; i < length; i++)
                {
                    var ingredient = _utilities.getIngredient(ingredients[i].idIngrediente, ingredients[i].tipoReceta);
                    if (ingredient.idProyecto != newRecipe.idProyecto)
                    {
                        return new ConflictObjectResult("El ingrediente " + ingredient.nombre + ", no pertenece al mismo proyecto que la receta");
                    }
                }
            }

            //Asigno costo total
            newRecipe = _utilities.UpdateRecipeCosts(newRecipe);

            try
            {
                _recipeService.Create(newRecipe);
            }
            catch (Exception)
            {
                return new ConflictObjectResult("Hubo un problema al agregar la receta");
            }


            return new CreatedAtRouteResult("CreateRecipe", new { nombre = newRecipe.nombre.ToString() }, newRecipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost("addToRecipe/{id}", Name = "AddIngredientToRecipe")]
        public ActionResult<List<IngredientInRecipe>> AddIngredientsToRecipe(int id, ListOfIngInRecipe ingredients)
        {
            var recipe = _recipeService.Get(id);
            var length = ingredients.ingredientes.Count;

            if(recipe == null)
            {
                return new NotFoundObjectResult("La receta no existe");
            }

            for(int i = 0; i<length; i++)
            {
                var ingredientId = ingredients.ingredientes[i].idIngrediente;
                var ingredient = _utilities.getIngredient(ingredientId, ingredients.ingredientes[i].tipoReceta);

                for (int z = 0; z < recipe.ingredientes.Count; z++)
                {
                    if(recipe.ingredientes[z].idIngrediente == ingredients.ingredientes[i].idIngrediente && recipe.ingredientes[z].tipoReceta == ingredients.ingredientes[i].tipoReceta)
                    {
                        return new ConflictObjectResult("El ingrediente ya existe en la receta");
                    }
                }

                if(ingredient == null)
                {
                    return new NotFoundObjectResult("El ingrediente " + ingredientId + ", no existe");
                }

                if(ingredient.idProyecto != recipe.idProyecto)
                {
                    return new ConflictObjectResult("El ingrediente " + ingredient.Id + ", no es del mismo proyecto que la receta");
                }

                recipe.ingredientes.Add(ingredients.ingredientes[i]);
            }

            recipe = _utilities.UpdateRecipeCosts(recipe);

            _recipeService.Update(id, recipe);

            return new OkObjectResult(recipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("updatePos/{id}")]
        public ActionResult UpdatePos(int id, ListOfIngInRecipe newIngredients)
        {
            var recipe = _recipeService.Get(id);

            recipe.ingredientes = newIngredients.ingredientes;

            _recipeService.Update(id, recipe);

            return new OkObjectResult("Posicion actualizada");
        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("{id}")]
        public ActionResult<Recipe> Update(int id, Recipe newRecipe)
        {
            var oldRecipe = _recipeService.Get(id);
            newRecipe.ingredientes = new List<IngredientInRecipe>();
            var serviceCost = _serviceCostService.Get(newRecipe.costoServicio);
            var category = _categoryService.Get(newRecipe.categoria);

            if (newRecipe.ingredientes.Count > 0)
            {
                return new ConflictObjectResult("No se permite modificacion de ingredientes en este metodo");
            }

            if (category == null)
            {
                return new NotFoundObjectResult("La categoria seleccionada no existe");
            }

            if (oldRecipe == null)
            {
                return new NotFoundObjectResult("No se encontro la receta" + id);
            }

            if(newRecipe.costoServicio != 0)
            {
                if (serviceCost.idProyecto != newRecipe.idProyecto)
                {
                    return new ConflictObjectResult("El costo de servicio: " + serviceCost.idProyecto + ", no pertenece a este proyecto");
                }
            }

            newRecipe.ingredientes = oldRecipe.ingredientes;
            newRecipe.Id = id;

            newRecipe = _utilities.UpdateRecipeCosts(newRecipe);

            _recipeService.Update(id, newRecipe);

            return new OkObjectResult(newRecipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("ingredientFromRecipe/{recipeId}")]
        public ActionResult<Recipe> UpdateIngredient(int recipeId, IngredientInRecipe newIng)
        {
            var recipe = _recipeService.Get(recipeId);
            var ingredient = _ingredientService.Get(newIng.idIngrediente);
            var ingredients = recipe.ingredientes;
            var length = recipe.ingredientes.Count;
            var found = false;

            if (recipe == null || ingredient == null)
            {
                return new NotFoundObjectResult("La receta o el ingrediente no existen");
            }

            if(ingredient.idProyecto != recipe.idProyecto)
            {
                return new ConflictObjectResult("El ingrediente no esta en el mismo proyecto que la receta");
            }

            for(int i = 0; i<length; i++)
            {
                if(recipe.ingredientes[i].idIngrediente == newIng.idIngrediente && recipe.ingredientes[i].tipoReceta == newIng.tipoReceta)
                {
                    recipe.ingredientes[i] = newIng;
                    found = true;
                }
            }

            if (!found)
            {
                return new NotFoundObjectResult("El Ingrediente no existe dentro de la receta");
            }

            recipe = _utilities.UpdateRecipeCosts(recipe);
            _recipeService.Update(recipeId, recipe);

            return new OkObjectResult(recipe);
        }

        [HttpPut("updateImage/{id}")]
        public ActionResult updateImage(int id, ProfilePicture image)
        {
            var recipe = _recipeService.Get(id);
            string filePath;

            if (recipe == null)
            {
                return new NotFoundObjectResult("No existe la receta: " + id);
            }

            if (image.base64 == null)
            {
                return new ConflictObjectResult("Formato de imagen no valido");
            }

            try
            {
                filePath = "/var/img/recipeimg_" + recipe.Id + ".png";
                File.WriteAllBytes(filePath, Convert.FromBase64String(image.base64));
                recipe.imagen = filePath;

                _recipeService.Update(id, recipe);
            }
            catch (Exception)
            {
                return new ConflictObjectResult("Ocurrio un error al guardar la imagen");
            }

            return new OkObjectResult(filePath);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var recipe = _recipeService.Get(id);

            if(recipe == null)
            {
                return new NotFoundObjectResult("La receta a eliminar no existe");
            }

            _recipeService.Remove(id);
            var filePath = "/var/img/recipeimg_" + recipe.Id + ".png";
            File.Delete(filePath);

            return new OkObjectResult(id);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("ingredientsFromRecipe/{id}")]
        public ActionResult DeleteAllIngredientsFromRecipe(int id)
        {
            var recipe = _recipeService.Get(id);

            if(recipe == null)
            {
                return new NotFoundObjectResult("La receta no existe");
            }

            recipe.ingredientes.RemoveAll(IngredientInRecipe => true);
            recipe.costoTotal = recipe.costoServicio;
            _recipeService.Update(id, recipe);

            return new OkObjectResult(recipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("oneIngFromRecipe/{recipeId}/{ingredientId}/{tipoReceta}")]
        public ActionResult DeleteOneIngredientFromRecipe(int recipeId, int ingredientId, int tipoReceta)
        {
            var recipe = _recipeService.Get(recipeId);
            var ingredient = _utilities.getIngredient(ingredientId, tipoReceta);
            var length = recipe.ingredientes.Count;
            var deleted = false;

            if(recipe == null || ingredient == null)
            {
                return new NotFoundObjectResult("La receta o el ingrediente no existe");
            }

            if(recipe.idProyecto != ingredient.idProyecto)
            {
                return new ConflictObjectResult("El ingrediente " + ingredient.Id + ", no pertenece al mismo proyecto que la receta");
            }

            for(int i = 0; i < length; i++)
            {
                if(recipe.ingredientes[i].idIngrediente == ingredientId && recipe.ingredientes[i].tipoReceta == tipoReceta )
                {
                    recipe.ingredientes.RemoveAt(i);
                    deleted = true;
                }

                if (deleted) { break; }
            }

            if (!deleted)
            {
                return new NotFoundObjectResult("No se encontro el ingrediente a eliminar en la receta");
            }

            recipe = _utilities.UpdateRecipeCosts(recipe);
            _recipeService.Update(recipeId, recipe);

            return new OkObjectResult(recipe);
        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpGet("prueba/{proyecto}/{ingrediente}")]
        public ActionResult getPrueba(int proyecto, int ingrediente)
        {
            _recipeService.GetIngredientInRecipe(proyecto, ingrediente);

            return null;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPatch]
        public ActionResult updatePrice()
        {
            var recipes = _recipeService.Get();

            for(int i=0; i < recipes.Count; i++)
            {
                recipes[i].precio = 100;
                _recipeService.Update(recipes[i].Id, recipes[i]);
            }

            var newRecipes = _recipeService.Get();

            return new OkObjectResult(newRecipes);
        }
    }
}
