﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class ConsumablesController
    {
        private readonly ConsumableService _consumableService;
        private readonly ProjectService _projectService;
        private readonly Utilities _utilities;

        public ConsumablesController(ConsumableService consumableService, ProjectService projectService, Utilities utilities)
        {
            _consumableService = consumableService;
            _projectService = projectService;
            _utilities = utilities;
        }
        
        [HttpGet]
        public ActionResult<List<Consumable>> Get() =>
            _consumableService.Get();

        [HttpGet("{id}")]
        public ActionResult<Consumable> Get(int id)
        {
            var consumable = _consumableService.Get(id);

            if(consumable == null)
            {
                return new NotFoundObjectResult("No existe el insumo " + id);
            }

            return new OkObjectResult(consumable);
        }

        [HttpGet("fromProject/{id}")]
        public ActionResult<List<Consumable>> GetByProject(int id)
        {
            var project = _projectService.Get(id);

            if(project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + id);
            }

            var consumable = _consumableService.GetByProject(id);

            if(consumable.Count == 0)
            {
                return new NotFoundObjectResult("El proyecto: " + id + ", no tiene asignado ningun insumo");
            }

            return new OkObjectResult(consumable);
        }

        [HttpGet("byName/{name}/{projectId}")]
        public ActionResult<List<Consumable>> GetByName(string name, int projectId)
        {
            var project = _projectService.Get(projectId);

            if(project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + projectId);
            }

            var consumables = _consumableService.GetByName(name, projectId);

            if(consumables.Count == 0)
            {
                return new NotFoundObjectResult("No existen coincidencias en este proyecto");
            }

            return new OkObjectResult(consumables);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost(Name = "CreateConsumable")]
        public ActionResult<Consumable> Create (Consumable newConsumable)
        {
            var project = _projectService.Get(newConsumable.idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto:" + newConsumable.idProyecto + ", no existe");
            }

            try
            {
                _consumableService.Create(newConsumable);
            }
            catch (Exception)
            {
                return new ConflictObjectResult("Hubo un problema al crear el insumo");
            }

            return new CreatedAtRouteResult("CreateConsumable", new { id = newConsumable.Id.ToString() }, newConsumable);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("{id}")]
        public ActionResult<Consumable> Update(int id, Consumable newConsumable)
        {
            var project = _projectService.Get(newConsumable.idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto: " + newConsumable.idProyecto + ", no existe");
            }

            var oldConsumable = _consumableService.Get(id);

            if(oldConsumable == null)
            {
                return new NotFoundObjectResult("El insumo a actualizar no existe");
            }

            newConsumable.Id = oldConsumable.Id;
            _consumableService.Update(id, newConsumable);

            //Actualizo costos de servicio y recetas
            _utilities.updateCostServiceAndRecipeWhenConsumable(id);

            return new OkObjectResult(newConsumable);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("{id}")]
        public ActionResult Delete (int id)
        {
            var consumable = _consumableService.Get(id);
            
            if(consumable == null)
            {
                return new NotFoundObjectResult("El insumo a eliminar no existe");
            }

            _consumableService.Remove(id);
            //Elimino insumo de los costos de servicio y actualizo recetas, costos
            _utilities.removeConsumableFromServiceCostAndRecipes(id);

            return new OkResult();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("removeFromProject/{id}")]
        public ActionResult DeleteFromProject(int id)
        {
            var project = _projectService.Get(id);
            
            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto: " + id + ", no existe");
            }

            var consumables = _consumableService.GetByProject(id);

            if(consumables.Count == 0)
            {
                return new NotFoundObjectResult("El proyecto: " + id + ", no tiene insumos asignados");
            }

            _consumableService.RemoveConsumablesFromProject(id);

            //Elimino insumos de costos de servicio y recetas, y actualiza costos
            for(int i = 0; i < consumables.Count; i++)
            {
                _utilities.removeConsumableFromServiceCostAndRecipes(consumables[i].Id);
            }

            return new OkResult();
        }
    }
}
