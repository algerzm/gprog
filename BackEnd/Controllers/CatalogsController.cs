﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class CatalogsController
    {
        private readonly CatalogService _catalogService;

        public CatalogsController(CatalogService catalogService)
        {
            _catalogService = catalogService;
        }

        [HttpGet("allTypes")]
        public ActionResult<List<TypesOfRecipe>> GetAllTypes()
        {
            var types = _catalogService.GetTypes();
            if(types.Count == 0)
            {
                return new NotFoundObjectResult("No hay tipos de recetas");
            }

            return new OkObjectResult(types);
        }

        [HttpGet("typeById/{id}")]
        public ActionResult<TypesOfRecipe> GetType(int id)
        {
            var type = _catalogService.GetTypeId(id);

            if(type == null)
            {
                return new NotFoundObjectResult("No existe el tipo de receta: " + id);
            }

            return new OkObjectResult(type);
        }

        [HttpGet("allUnits")]
        public ActionResult<List<MeasureUnits>> GetAllUnits()
        {
            var units = _catalogService.GetMeasureUnits();
            if (units.Count == 0)
            {
                return new NotFoundObjectResult("No hay unidades de medida");
            }

            return new OkObjectResult(units);
        }

        [HttpGet("unitById/{id}")]
        public ActionResult<MeasureUnits> GetUnit(int id)
        {
            var unit = _catalogService.GetUnitId(id);

            if (unit == null)
            {
                return new NotFoundObjectResult("No existe la unidad de medida: " + id);
            }

            return new OkObjectResult(unit);
        }

        [HttpGet("allRoles")]
        public ActionResult<List<Role>> GetAllRoles()
        {
            var roles = _catalogService.GetRoles();

            if(roles.Count == 0)
            {
                return new NotFoundObjectResult("No existen roles");
            }

            return new OkObjectResult(roles);
        }

        [HttpGet("roleById/{id}")]
        public ActionResult<Role> GetRole(int id)
        {
            var role = _catalogService.GetRoleId(id);

            if(role == null)
            {
                return new NotFoundObjectResult("No existe el rol: " + id);
            }

            return new OkObjectResult(role);
        }
    }
}
