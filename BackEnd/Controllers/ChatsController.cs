﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class ChatsController
    {
        private readonly ChatService _chatService;
        private readonly ProjectService _projectService;
        private readonly UserService _userService;

        public ChatsController(ChatService chatService, ProjectService projectService, UserService userService)
        {
            _chatService = chatService;
            _projectService = projectService;
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<List<Chat>> Get() =>
            _chatService.Get();

        [HttpGet("{id}")]
        public ActionResult<Chat> Get(int id)
        {
            var chat = _chatService.Get(id);

            if(chat == null)
            {
                return new NotFoundObjectResult("El chat: " + id + ", no existe");
            }

            return chat;
        }

        [HttpGet("chatByProject/{id}")]
        public ActionResult<List<ChatWithUsers>> GetByProject(int id)
        {
            var project = _projectService.Get(id);

            if (project == null)
            {
                return new NotFoundObjectResult("No se encuentra el proyecto: " + id);
            }

            if (project.archivado == true)
            {
                return new NotFoundObjectResult("No hay proyectos activos con el id: " + id);
            }

            var chat = _chatService.GetByProject(id);

            if (chat == null)
            {
                return new NotFoundObjectResult("No existe chat en el proyecto: " + id);
            }

            List<ChatWithUsers> chatUsers = new List<ChatWithUsers>();
            List<Users> allUsers = new List<Users>();

            allUsers = _userService.Get();

            for(int i=0; i < chat.mensajes.Count; i++)
            {
                var user = allUsers.Find(User => User.Id == chat.mensajes[i].idUsuario);
                if(user != null)
                {
                    ChatWithUsers chatWithUser = new ChatWithUsers();
                    chatWithUser.mensaje = chat.mensajes[i].mensaje;
                    chatWithUser.idUsuario = chat.mensajes[i].idUsuario;
                    chatWithUser.usuario = user;
                    chatWithUser.fecha = chat.mensajes[i].fecha;

                    chatUsers.Add(chatWithUser);
                }
            }
            

            return new OkObjectResult(chatUsers);
        }
    }
}
