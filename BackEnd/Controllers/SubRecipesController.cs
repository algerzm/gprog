﻿using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class SubRecipesController
    {
        private readonly SubRecipeService _subRecipeService;
        private readonly IngredientService _ingredientService;
        private readonly ProjectService _projectService;
        private readonly RecipeService _recipeService;
        private readonly Utilities _utilities;

        public SubRecipesController(SubRecipeService subRecipeService, IngredientService ingredientService, ProjectService projectService,RecipeService recipeService, Utilities utilities)
        {
            _subRecipeService = subRecipeService;
            _ingredientService = ingredientService;
            _projectService = projectService;
            _recipeService = recipeService;
            _utilities = utilities;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpGet]
        public ActionResult<List<SubRecipe>> Get() =>
            _subRecipeService.Get();
        
        [HttpGet("{id}")]
        public ActionResult<SubRecipe> Get(int id)
        {
            var subRecipe = _subRecipeService.Get(id);

            if (subRecipe == null)
            {
                return new NotFoundObjectResult("No se encontro la sub recera: " + id);
            }

            subRecipe.imagen = _utilities.getImage(2, id);

            return new OkObjectResult(subRecipe);
        }
        
        [HttpGet("fromProject/{projectId}")]
        public ActionResult<List<SubRecipe>> GetFromProject(int projectId)
        {
            var subRecipe = _subRecipeService.GetByProject(projectId);

            if (subRecipe.Count == 0)
            {
                return new NotFoundObjectResult("No hay sub recetas asignadas en este proyecto");
            }

            for(int i =0; i < subRecipe.Count; i++)
            {
                subRecipe[i].imagen = _utilities.getImage(2, subRecipe[i].Id);
            }

            return new OkObjectResult(subRecipe);
        }

        [HttpGet("ingFromSubRecipe/{subRecipeId}")]
        public ActionResult<List<IngredientInRecipe>> GetIngredientsFromSubRecipe(int subRecipeId)
        {
            var subRecipe = _subRecipeService.Get(subRecipeId);

            if (subRecipe == null)
            {
                return new NotFoundObjectResult("No existe la receta");
            }

            if (subRecipe.ingredientes.Count == 0)
            {
                return new NotFoundObjectResult("No hay ingredientes asignados a esta sub receta");
            }

            return subRecipe.ingredientes;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpPost("nombre", Name = "GetSubRecipeByName")]
        public ActionResult<List<SubRecipe>> Get(SearchRecipeName searchRecipe)
        {
            var subRecipe = _subRecipeService.GetByName(searchRecipe.nombre, searchRecipe.idProyecto);

            if (subRecipe.Count == 0)
            {
                return new NotFoundObjectResult("No hay coincidencias en el proyecto " + searchRecipe.idProyecto);
            }

            return new OkObjectResult(subRecipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3")]
        [HttpPost(Name = "CreateSubRecipe")]
        public ActionResult<SubRecipe> Create(SubRecipe newSubRecipe)
        {
            var duplicatedSubRecipe = _subRecipeService.GetByName(newSubRecipe.nombre, newSubRecipe.idProyecto);
            var ingredients = newSubRecipe.ingredientes;
            var project = _projectService.Get(newSubRecipe.idProyecto);

            if (project == null)
            {
                return new NotFoundObjectResult("El proyecto " + newSubRecipe.idProyecto + ", no existe");
            }

            if (duplicatedSubRecipe.Count > 0)
            {
                return new ConflictObjectResult("Esta sub receta ya existe en este proyecto");
            }

            if (ingredients.Count > 0)
            {
                var length = ingredients.Count;
                for (int i = 0; i < length; i++)
                {
                    var ingredient = _utilities.getIngredient(ingredients[i].idIngrediente, ingredients[i].tipoReceta);
                    if (ingredient.idProyecto != newSubRecipe.idProyecto)
                    {
                        return new ConflictObjectResult("El ingrediente " + ingredient.nombre + ", no pertenece al mismo proyecto que la sub receta");
                    }
                }
            }

            newSubRecipe = _utilities.UpdateSubRecipeCosts(newSubRecipe);

            try
            {
                _subRecipeService.Create(newSubRecipe);
            }
            catch (Exception)
            {
                return new ConflictObjectResult("Hubo un problema al agregar la sub receta");
            }


            return new CreatedAtRouteResult("CreateSubRecipe", new { nombre = newSubRecipe.nombre.ToString() }, newSubRecipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3")]
        [HttpPost("addToSubRecipe/{id}", Name = "AddIngredientToSubRecipe")]
        public ActionResult<List<IngredientInRecipe>> AddIngredientsToRecipe(int id, ListOfIngInRecipe ingredients)
        {
            var subRecipe = _subRecipeService.Get(id);
            var length = ingredients.ingredientes.Count;

            if (subRecipe == null)
            {
                return new NotFoundObjectResult("La sub receta no existe");
            }

            for (int i = 0; i < length; i++)
            {
                var ingredientId = ingredients.ingredientes[i].idIngrediente;
                var ingredient = _utilities.getIngredient(ingredientId, ingredients.ingredientes[i].tipoReceta);
                
                if (ingredient == null)
                {
                    return new NotFoundObjectResult("El ingrediente " + ingredientId + ", no existe");
                }

                for (int z = 0; z < subRecipe.ingredientes.Count; z++)
                {
                    if ((subRecipe.ingredientes[z].idIngrediente == ingredients.ingredientes[i].idIngrediente) && (subRecipe.ingredientes[z].tipoReceta == ingredients.ingredientes[i].tipoReceta))
                    {
                        return new ConflictObjectResult("El ingrediente ya existe en la sub receta");
                    }
                }

                if (ingredient.idProyecto != subRecipe.idProyecto)
                {
                    return new ConflictObjectResult("El ingrediente " + ingredient.Id + ", no es del mismo proyecto que la sub receta");
                }

                subRecipe.ingredientes.Add(ingredients.ingredientes[i]);
            }
            
            subRecipe = _utilities.UpdateSubRecipeCosts(subRecipe);

            _subRecipeService.Update(id, subRecipe);

            //Actualizo costos de recetas y subrecetas
            _utilities.UpdateRecipeCostWhenSubRecipe(subRecipe);
            _utilities.UpdateSubRecipeCostWhenSubRecipe(subRecipe);

            return new OkObjectResult(subRecipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3")]
        [HttpPut("{id}")]
        public ActionResult<SubRecipe> Update(int id, SubRecipe newSubRecipe)
        {
            var oldSubRecipe = _subRecipeService.Get(id);
            newSubRecipe.ingredientes = new List<IngredientInRecipe>();

            if (newSubRecipe.ingredientes.Count > 0)
            {
                return new ConflictObjectResult("No se permite modificacion de ingredientes en este metodo");
            }

            if (oldSubRecipe == null)
            {
                return new NotFoundObjectResult("No se encontro la sub receta" + id);
            }

            newSubRecipe.ingredientes = oldSubRecipe.ingredientes;
            newSubRecipe.Id = id;
            newSubRecipe = _utilities.UpdateSubRecipeCosts(newSubRecipe);
            newSubRecipe.costoSubrecetaUnidad = newSubRecipe.costoTotal / newSubRecipe.unidadesProducidas;

            _subRecipeService.Update(id, newSubRecipe);

            //Actualizo costos de recetas y subrecetas
            _utilities.UpdateRecipeCostWhenSubRecipe(newSubRecipe);
            _utilities.UpdateSubRecipeCostWhenSubRecipe(newSubRecipe);

            return new OkObjectResult(newSubRecipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3")]
        [HttpPut("ingredientFromSubRecipe/{recipeId}")]
        public ActionResult<SubRecipe> UpdateIngredient(int recipeId, IngredientInRecipe newIng)
        {
            var subRecipe = _subRecipeService.Get(recipeId);
            var ingredient = _utilities.getIngredient(newIng.idIngrediente, newIng.tipoReceta);
            var ingredients = subRecipe.ingredientes;
            var length = subRecipe.ingredientes.Count;
            var found = false;

            if (subRecipe == null || ingredient == null)
            {
                return new NotFoundObjectResult("La sub receta o el ingrediente no existen");
            }

            if (ingredient.idProyecto != subRecipe.idProyecto)
            {
                return new ConflictObjectResult("El ingrediente no esta en el mismo proyecto que la sub receta");
            }

            for (int i = 0; i < length; i++)
            {
                if ((subRecipe.ingredientes[i].idIngrediente == newIng.idIngrediente)&&(subRecipe.ingredientes[i].tipoReceta == newIng.tipoReceta))
                {
                    subRecipe.ingredientes[i] = newIng;
                    found = true;
                }
            }

            if (!found)
            {
                return new NotFoundObjectResult("El Ingrediente no existe dentro de la receta");
            }

            subRecipe = _utilities.UpdateSubRecipeCosts(subRecipe);

            _subRecipeService.Update(recipeId, subRecipe);

            //Actualizo costos de recetas y subrecetas
            _utilities.UpdateRecipeCostWhenSubRecipe(subRecipe);
            _utilities.UpdateSubRecipeCostWhenSubRecipe(subRecipe);

            return new OkObjectResult(subRecipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3")]
        [HttpPut("updateImage/{subrecipeId}")]
        public ActionResult<SubRecipe> UpdateImage(int subRecipeId, ProfilePicture image)
        {
            var subRecipe = _subRecipeService.Get(subRecipeId);

            if(subRecipe == null)
            {
                return new NotFoundObjectResult("No exite la sub receta: " + subRecipeId);
            }

            if (image.base64 == null)
            {
                return new ConflictObjectResult("Formato de imagen no valido");
            }

            var imgName = "subrecipeimg_" + subRecipeId;

            _utilities.saveImage(imgName, image.base64);

            return new OkObjectResult(subRecipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("updatePos/{id}")]
        public ActionResult UpdatePos(int id, ListOfIngInRecipe newIngredients)
        {
            var subrecipe = _subRecipeService.Get(id);

            subrecipe.ingredientes = newIngredients.ingredientes;

            _subRecipeService.Update(id, subrecipe);

            return new OkObjectResult("Posicion actualizada");
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3")]
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var subRecipe = _subRecipeService.Get(id);

            if (subRecipe == null)
            {
                return new NotFoundObjectResult("La sub receta a eliminar no existe");
            }

            //Elimino sub recetas y actualizo costos
            _utilities.RemoveSubRecipes(subRecipe);

            _subRecipeService.Remove(id);

            return new OkObjectResult(id);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("ingredientsFromSubRecipe/{id}")]
        public ActionResult DeleteAllIngredientsFromSubRecipe(int id)
        {
            var subRecipe = _subRecipeService.Get(id);

            if (subRecipe == null)
            {
                return new NotFoundObjectResult("La sub receta no existe");
            }

            subRecipe.ingredientes.RemoveAll(IngredientInRecipe => true);
            subRecipe.costoTotal = 0;
            subRecipe.costoSubrecetaUnidad = 0;
            _subRecipeService.Update(id, subRecipe);

            //Actualizo costos de recetas y subrecetas
            _utilities.UpdateRecipeCostWhenSubRecipe(subRecipe);
            _utilities.UpdateSubRecipeCostWhenSubRecipe(subRecipe);

            return new OkObjectResult(subRecipe);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("oneIngFromSubRecipe/{recipeId}/{ingredientId}/{tipoReceta}")]
        public ActionResult DeleteOneIngredientFromRecipe(int recipeId, int ingredientId, int tipoReceta)
        {
            var subRecipe = _subRecipeService.Get(recipeId);
            var ingredient = _utilities.getIngredient(ingredientId, tipoReceta);
            var deleted = false;

            if (subRecipe == null || ingredient == null)
            {
                return new NotFoundObjectResult("La sub receta o el ingrediente no existe");
            }

            var length = subRecipe.ingredientes.Count;

            if (subRecipe.idProyecto != ingredient.idProyecto)
            {
                return new ConflictObjectResult("El ingrediente " + ingredient.Id + ", no pertenece al mismo proyecto que la sub receta");
            }

            for (int i = 0; i < length; i++)
            {
                if (subRecipe.ingredientes[i].idIngrediente == ingredientId && subRecipe.ingredientes[i].tipoReceta == tipoReceta)
                {
                    subRecipe.ingredientes.RemoveAt(i);
                    deleted = true;
                }

                if (deleted) { break; }
            }

            if (!deleted)
            {
                return new NotFoundObjectResult("No se encontro el ingrediente a eliminar en la sub receta");
            }

            subRecipe = _utilities.UpdateSubRecipeCosts(subRecipe);

            _subRecipeService.Update(recipeId, subRecipe);

            //Actualizo costos de recetas y subrecetas
            _utilities.UpdateRecipeCostWhenSubRecipe(subRecipe);
            _utilities.UpdateSubRecipeCostWhenSubRecipe(subRecipe);

            return new OkObjectResult(subRecipe);
        }
    }
}
