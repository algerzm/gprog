﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class FixedCostsController
    {
        private readonly FixedCostService _fixedCostService;
        private readonly ProjectService _projectService;

        public FixedCostsController(FixedCostService fixedCostService, ProjectService projectService)
        {
            _fixedCostService = fixedCostService;
            _projectService = projectService;
        }
        
        [HttpGet]
        public ActionResult<List<FixedCost>> Get() =>
            _fixedCostService.Get();

        [HttpGet("{id}", Name = "GetFixedCostById")]
        public ActionResult<FixedCost> Get(int id)
        {
            var fixedCost = _fixedCostService.Get(id);

            if(fixedCost == null)
            {
                return new NotFoundResult();
            }

            return fixedCost;
        }

        [HttpGet("fromProject/{id}")]
        public ActionResult<List<FixedCost>> GetByProject(int id)
        {
            var project = _projectService.Get(id);

            if(project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + id);
            }

            var fixedCosts = _fixedCostService.GetByProject(id);

            if (fixedCosts.Count == 0)
            {
                return new NotFoundObjectResult("El proyecto: " + id + ", no tiene asignado ningun costo fijo");
            }

            return new OkObjectResult(fixedCosts);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost("nombre", Name = "GetFixedCostByName")]
        public ActionResult<List<FixedCost>> Get(SearchFixedCostName fixedCostIn)
        {
            var fixedCosts = _fixedCostService.GetByName(fixedCostIn.concepto, fixedCostIn.idProyecto);

            if (fixedCosts.Count == 0)
            {
                return new NotFoundResult();
            }

            return fixedCosts;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost]
        public ActionResult<FixedCost> Create(FixedCost newFixedCost)
        {
            var project = _projectService.Get(newFixedCost.idProyecto);

            if(project == null)
            {
                return new NotFoundResult();
            }

            try
            {
                _fixedCostService.Create(newFixedCost);
            }
            catch(Exception e)
            {
                return new ConflictResult();
            }

            return new CreatedAtRouteResult("GetFixedCostById", new { id = newFixedCost.Id.ToString() }, newFixedCost);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("{id}")]
        public IActionResult Update(int id, FixedCost newFixedCost)
        {
            var project = _projectService.Get(newFixedCost.idProyecto);

            if (project == null)
            {
                return new NotFoundResult();
            }

            var oldFixedCost = _fixedCostService.Get(id);

            if(oldFixedCost == null)
            {
                return new NotFoundResult();
            }

            newFixedCost.Id = id;
            _fixedCostService.Update(id, newFixedCost);

            return new NoContentResult();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("{id}")]
        public IActionResult Delete (int id)
        {
            var fixedCost = _fixedCostService.Get(id);

            if(fixedCost == null)
            {
                return new NotFoundResult();
            }

            _fixedCostService.Remove(id);

            return new NoContentResult();
        }
    }
}
