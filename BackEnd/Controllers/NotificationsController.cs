﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GProGApi.Services;
using GProGApi.Models;


namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3")]
    public class NotificationsController
    {
        private readonly NotificationService _notificationService;
        private readonly UserService _userService;

        public NotificationsController(NotificationService notificationService, UserService userService)
        {
            _notificationService = notificationService;
            _userService = userService;
        }

        [HttpGet("pendingNotifications/{id}")]
        public ActionResult<List<Notification>> GetPendingNotifications(int id)
        {
            var user = _userService.Get(id);
            List<Notification> pendingNotifications = new List<Notification>();

            if(user == null)
            {
                return new NotFoundObjectResult("No existe el usuario: " + id);
            }

            var controlOfUser = _notificationService.GetNotificationsReadedOfUser(id);
            List<int> notificationsReaded = new List<int>();

            for(int i=0; i < controlOfUser.Count; i++)
            {
                notificationsReaded.Add(controlOfUser[i].idNotificacion);
            }
            
            if(controlOfUser.Count != 0)
            {
                pendingNotifications = _notificationService.GetNotificationsExceptList(notificationsReaded);

                if (pendingNotifications.Count == 0)
                {
                    return new NotFoundObjectResult("El usuario: " + id + ", no tiene notificaciones por leer");
                }
            }

            return pendingNotifications;
        }
        
        [HttpPost("notificationReaded")]
        public ActionResult<NotificationReaded> addNotificationReaded (NotificationReaded newRead)
        {
            var notification = _notificationService.Get(newRead.idNotificacion);
            var user = _userService.Get(newRead.idUsuario);

            if(notification == null)
            {
                return new NotFoundObjectResult("La notificacion " + newRead.idNotificacion + ", no existe,notification");
            }

            if(user == null)
            {
                return new NotFoundObjectResult("El usuario: " + newRead.idUsuario + ", no existe,user");
            }

            var oldRead = _notificationService.GetNotificationReaded(newRead.idUsuario, newRead.idNotificacion);

            if(oldRead == null)
            {
                _notificationService.ReadedNotification(newRead);
            }

            return new OkObjectResult(newRead);
        }
    }
}
