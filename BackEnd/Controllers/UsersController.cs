﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class UsersController
    {
        private readonly UserService _userService;
        private readonly ProjectService _projectService;
        private readonly NotificationService _notificationService;

        public UsersController(UserService userService, ProjectService projectService, NotificationService notificationService)
        {
            _userService = userService;
            _projectService = projectService;
            _notificationService = notificationService;
        }

        [HttpGet("validate")]
        public ActionResult validateToken()
        {
            return new OkObjectResult(true);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpGet]
        public ActionResult<List<Users>> Get() =>
            _userService.Get();
        
        [HttpGet("safe")]
        public ActionResult<List<Users>> GetWithNoPassword()
        {
            var users = _userService.Get();
            if(users.Count == 0)
            {
                return new NotFoundObjectResult("No existen usuarios");
            }

            for(int i = 0; i < users.Count; i++)
            {
                users[i].password = "No Password";
            }

            return new OkObjectResult(users);
        }

        [HttpGet("getPicture/{id}")]
        public ActionResult<ProfilePicture> getProfilePicture(int id)
        {
            var user = _userService.Get(id);

            if(user == null)
            {
                return new NotFoundObjectResult("No se encontro el usuario: " + id);
            }

            ProfilePicture picture = new ProfilePicture();

            try
            {
                var filePath = "/var/img/profilepic_" + id + ".png";

                if (File.Exists(filePath))
                {
                    Byte[] bytes = File.ReadAllBytes(filePath);

                    picture.base64 = Convert.ToBase64String(bytes);
                }
                else
                {
                    Byte[] bytes = File.ReadAllBytes("/var/img/default.png");
                    picture.base64 = Convert.ToBase64String(bytes);
                }
                
            }
            catch(Exception e)
            {
                return new ConflictObjectResult("Ocurrio un error al obtener la imagen: " + e.ToString());
            }

            return new OkObjectResult(picture);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpGet("{id}", Name = "GetUser")]
        public ActionResult<Users> Get(int id)
        {
            var user = _userService.Get(id);

            if (user == null)
            {
                return new NotFoundResult();
            }

            return user;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpPost]
        public ActionResult<Users> Create(Users user)
        {
            Preferences preferencias = new Preferences();
            var userWithEmail = _userService.GetByEmail(user.correo);

            if (userWithEmail != null)
            {
                return new ConflictObjectResult("El correo: " + user.correo + ", ya esta registrado");
            }

            preferencias.idioma = "en";
            preferencias.notificaciones = true;
            preferencias.tema = "default";

            user.preferencias = preferencias;
            user.fechaAlta = DateTime.UtcNow.Date;
            _userService.Create(user);

            return new CreatedAtRouteResult("GetUser", new { id = user.Id.ToString() }, user);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpPut("{id}")]
        public IActionResult Update(int id, Users userIn)
        {
            var user = _userService.Get(id);
            
            if(user == null)
            {
                return new NotFoundResult();
            }

            userIn.Id = id;
            _userService.Update(id, userIn);

            return new NoContentResult();
        }

        [HttpPut("changePassword/{id}")]
        public ActionResult ChangePassword(int id, ChangePasword passwords)
        {
            var user = _userService.Get(id);

            if(user == null)
            {
                return new NotFoundObjectResult("No existe el usuario: " + id);
            }

            if(passwords.newPassword == "")
            {
                return new ConflictObjectResult("Erorr, contraseña invalida");
            }

            if(passwords.password != user.password)
            {
                return new ConflictObjectResult("Error: Contraseña actual incorrecta");
            }

            user.password = passwords.newPassword;

            _userService.Update(id, user);

            return new OkResult();
        }

        [HttpPut("updatePreferences/{id}")]
        public ActionResult<Users> updatePreferences(int id, Preferences preferences)
        {
            var user = _userService.Get(id);

            if(user == null)
            {
                return new NotFoundObjectResult("El usuario: " + id + ", no existe");
            }

            user.preferencias = preferences;

            _userService.Update(id, user);
            user.password = "No password";

            return new OkObjectResult(user);
        }

        [HttpPut("updatePicture/{id}")]
        public ActionResult updateProfilePicture(int id, ProfilePicture picture)
        {
            var user = _userService.Get(id);
            string filePath;
            if (user == null)
            {
                return new NotFoundObjectResult("No se encontro el usuario: " + id);
            }

            if(picture.base64 == null)
            {
                return new NotFoundObjectResult("Formato de imagen no valido");
            }

            try
            {
                filePath = "/var/img/profilepic_" + user.Id + ".png";
                File.WriteAllBytes(filePath, Convert.FromBase64String(picture.base64));
            }
            catch (Exception)
            {
                return new ConflictObjectResult("Ocurrio un error al guardar la imagen");
            }

            return new OkObjectResult(filePath);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = _userService.Get(id);

            if(user == null)
            {
                return new NotFoundResult();
            }

            _projectService.RemoveUserFromProject(user.Id);
            _userService.Remove(user.Id);
            _notificationService.RemoveUsersFromReadedNotifications(id);
            _notificationService.RemoveNotificationsOfUser(id);

            var filePath = "/var/img/profilepic_" + id + ".png";
            File.Delete(filePath);

            return new NoContentResult();
        }
    }
}
