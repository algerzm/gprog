﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Services;
using GProGApi.Models;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class ServiceCostsController
    {
        private readonly ConsumableService _consumableService;
        private readonly ServiceCostService _serviceCostService;
        private readonly ProjectService _projectService;
        private readonly RecipeService _recipeService;
        private readonly Utilities _utilities;

        public ServiceCostsController(ConsumableService consumableService, ServiceCostService serviceCostService, ProjectService projectService, RecipeService recipeService, Utilities utilities)
        {
            _consumableService = consumableService;
            _serviceCostService = serviceCostService;
            _projectService = projectService;
            _recipeService = recipeService;
            _utilities = utilities;
        }

        [HttpGet]
        public ActionResult<List<ServiceCost>> Get() =>
            _serviceCostService.Get();

        [HttpGet("{id}", Name = "GetById")]
        public ActionResult<ServiceCost> Get(int id)
        {
            var serviceCost = _serviceCostService.Get(id);
            
            if(serviceCost == null)
            {
                return new NotFoundObjectResult("El costo de servicio no existe");
            }

            return new OkObjectResult(serviceCost);
        }

        [HttpGet("consumablesFromService/{id}")]
        public ActionResult<List<ConsumableInServiceCost>> GetConsumables(int id)
        {
            var servicesCosts = _serviceCostService.GetConsumablesFromServiceCost(id);

            if(servicesCosts.Count == 0)
            {
                return new NotFoundObjectResult("No se encontraron insumos asignados al costo de servicio: " + id);
            }

            return new OkObjectResult(servicesCosts);
        }

        [HttpGet("fromProject/{id}")]
        public ActionResult<List<ServiceCost>> GetByProject(int id)
        {
            var project = _projectService.Get(id);

            if(project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + id);
            }

            var serviceCost = _serviceCostService.GetByProject(id);

            if(serviceCost.Count == 0)
            {
                return new NotFoundObjectResult("El proyecto: " + id + ",no tiene costos de servicio asignados");
            }

            return new OkObjectResult(serviceCost);
        }

        [HttpGet("recipesWithServiceCost/{id}")]
        public ActionResult<List<Recipe>> GetRecipesWithServiceCost(int id)
        {
            var serviceCost = _serviceCostService.Get(id);
            if(serviceCost == null)
            {
                return new NotFoundObjectResult("No existe el costo de servicio " + id);
            }

            var recipes = _recipeService.GetRecipesWithServiceCost(id);
            if (recipes.Count == 0)
            {
                return new NotFoundObjectResult("No hay recetas con este costo de servicio");
            }

            return new OkObjectResult(recipes);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost(Name = "CreateServiceCost")]
        public ActionResult<ServiceCost> Create(ServiceCost newServiceCost)
        {
            var project = _projectService.Get(newServiceCost.idProyecto);
            var oldService = _serviceCostService.GetByName(newServiceCost.nombre, newServiceCost.idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto: " + newServiceCost.idProyecto + ", no existe");
            }

            if (oldService.Count > 0)
            {
                return new ConflictObjectResult("El costo de servicio "+newServiceCost.nombre+", ya existe en el proyecto");
            }

            _serviceCostService.Create(newServiceCost);

            return new CreatedAtRouteResult("CreateServiceCost", new { id = newServiceCost.Id.ToString() }, newServiceCost);

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost("addConsumable")]
        public ActionResult<ConsumableInServiceCost> AddConsumableToService(ConsumableInServiceCost newConsumable)
        {
            var consumable = _consumableService.Get(newConsumable.idInsumo);
            var serviceCost = _serviceCostService.Get(newConsumable.idCostoServicio);
            var consumableInService = _serviceCostService.GetConsumable(consumable.Id, serviceCost.Id);
            
            if(consumable == null)
            {
                return new NotFoundObjectResult("El insumo: " + newConsumable.idInsumo + ", no existe");
            }

            if(serviceCost == null)
            {
                return new NotFoundObjectResult("El costo de servicio: " + newConsumable.idCostoServicio + ", no existe");
            }
            
            if(consumable.idProyecto != serviceCost.idProyecto)
            {
                return new ConflictObjectResult("El insumo y el costo de servicio no pertenecen al mismo proyecto");
            }

            if(consumableInService != null)
            {
                return new ConflictObjectResult("El insumo ya existe en el costo de servicio");
            }

            var project = _projectService.Get(consumable.idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto: " + consumable.idProyecto + ", no existe");
            }

            
            _serviceCostService.addToServiceCost(newConsumable);

            //Actualizo costo del costo de servicio
            _utilities.updateCostServiceAndRecipeWhenConsumable(newConsumable.idInsumo);

            return new OkObjectResult(newConsumable);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("{id}")]
        public ActionResult<ServiceCost> Update(int id, ServiceCost newServiceCost)
        {
            var project = _projectService.Get(newServiceCost.idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto no existe");
            }

            var oldServiceCost = _serviceCostService.Get(id);

            if(oldServiceCost == null)
            {
                return new NotFoundObjectResult("El costo de servicio no existe");
            }

            newServiceCost.Id = oldServiceCost.Id;
            newServiceCost.costo = oldServiceCost.costo;

            _serviceCostService.Update(id, newServiceCost);

            return new OkObjectResult(newServiceCost);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("{id}")]
        public ActionResult Delete (int id)
        {
            var serviceCost = _serviceCostService.Get(id);
            
            if(serviceCost == null)
            {
                return new NotFoundObjectResult("El costo de servicio no existe");
            }

            _serviceCostService.Remove(id);

            //Elimino costo de servicio de las recetas
            var recipes = _recipeService.GetRecipesWithServiceCost(id);
            _utilities.removeCostServiceFromRecipes(recipes);
            //Elimino relaciones de insumos y el costo de servicio
            _serviceCostService.RemoveAllConsumablesFromServiceCost(id);

            return new OkResult();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("removeConsumable/{idService}/{idConsumable}")]
        public ActionResult DeleteConsumable (int idService, int idConsumable)
        {
            var serviceCost = _serviceCostService.Get(idService);
            var consumable = _consumableService.Get(idConsumable);

            if(serviceCost == null || consumable == null)
            {
                return new NotFoundObjectResult("El costo de servicio o el insumo no existe");
            }

            _serviceCostService.RemoveConsumableFromServiceCost(idConsumable, idService);

            //Actualizo el costo del costo de servicio
            _utilities.updateCostFromServiceCost(serviceCost);

            //Actualizo las recetas que usaban ese costo de servicio
            var recipes = _recipeService.GetRecipesWithServiceCost(idService);
            if (recipes.Count > 0)
            {
                for(int i=0; i<recipes.Count; i++)
                {
                    var recipe = _utilities.UpdateRecipeCosts(recipes[i]);
                    _recipeService.Update(recipe.Id, recipe);
                }
            }


            return new OkResult();
        }
    }
}
