﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Amazon.S3.Model;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2,3,4")]
    [DisableRequestSizeLimit]
    public class ProjectsController
    {
        private readonly ProjectService _projectService;
        private readonly UserService _userService;
        private readonly IngredientService _ingredientService;
        private readonly SupplierService _supplierService;
        private readonly FixedCostService _fixedCostService;
        private readonly RecipeService _recipeService;
        private readonly ServiceCostService _serviceCostService;
        private readonly ProfitService _profitService;
        private readonly ConsumableService _consumableService;
        private readonly SubRecipeService _subRecipeService;
        private readonly ChatService _chatService;
        private readonly Utilities _utilities;
        private readonly string baseUrl;
        private readonly IAmazonS3 _s3Client;
        private readonly string _bucketName;


        public ProjectsController(ProjectService projectService, UserService userService, IngredientService ingredientService, SupplierService supplierService, FixedCostService fixedCostService, RecipeService recipeService, Utilities utilities, ServiceCostService serviceCostService, ProfitService profitService, ConsumableService consumableService, SubRecipeService subRecipeService, ChatService chatService, IAmazonS3 s3Client)
        {
            _projectService = projectService;
            _userService = userService;
            _ingredientService = ingredientService;
            _supplierService = supplierService;
            _fixedCostService = fixedCostService;
            _recipeService = recipeService;
            _serviceCostService = serviceCostService;
            _profitService = profitService;
            _consumableService = consumableService;
            _subRecipeService = subRecipeService;
            _chatService = chatService;
            _utilities = utilities;
            _s3Client = s3Client;
            _bucketName = Environment.GetEnvironmentVariable("AWS_BUCKET_NAME");

            Globals global = new Globals();
            baseUrl = global.getBaseUrl();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2")]
        [HttpGet]
        public ActionResult<List<Project>> Get()
        {
            var allProjects = _projectService.Get();
            List<ProjectWithUsers> withUsers = new List<ProjectWithUsers>();
            var allUsers = _userService.Get();

            for (int i = 0; i < allProjects.Count; i++)
            {
                ProjectWithUsers projectWithUsers = new ProjectWithUsers();
                var users = _projectService.GetUsersFromProject(allProjects[i].Id);

                projectWithUsers.Id = allProjects[i].Id;
                projectWithUsers.nombre = allProjects[i].nombre;
                projectWithUsers.duracion = allProjects[i].duracion;
                projectWithUsers.imagen = allProjects[i].imagen;
                projectWithUsers.descripcion = allProjects[i].descripcion;
                projectWithUsers.archivado = allProjects[i].archivado;
                projectWithUsers.infoCliente = allProjects[i].infoCliente;

                for (int x = 0; x < users.Count; x++)
                {
                    var userIn = allUsers.Find(a => a.Id == users[x].idUsuario);
                    userIn.password = "No Password";
                    projectWithUsers.usuarios.Add(userIn);
                }

                if (allProjects[i].archivado == false)
                {
                    withUsers.Add(projectWithUsers);
                }
            }

            return new OkObjectResult(withUsers);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3,4")]
        [HttpGet("getArchivedProjectsOfUser/{id}")]
        public ActionResult<List<Project>> GetArchivedProjects(int id)
        {
            var user = _userService.Get(id);

            if (user == null)
            {
                return new NotFoundObjectResult("No existe el usuario: " + id);
            }

            var projectsIds = _projectService.GetProjectsByUser(user.Id);

            if (projectsIds.Count == 0)
            {
                return new NotFoundObjectResult("El usuario no tiene proyectos asignados");
            }

            var count = projectsIds.Count;
            List<ProjectWithUsers> withUsers = new List<ProjectWithUsers>();
            var allUsers = _userService.Get();

            for (int i = 0; i < count; i++)
            {
                var idProyecto = projectsIds[i].idProyecto;
                var project = _projectService.Get(idProyecto);

                ProjectWithUsers projectWithUsers = new ProjectWithUsers();
                var users = _projectService.GetUsersFromProject(project.Id);

                projectWithUsers.Id = project.Id;
                projectWithUsers.nombre = project.nombre;
                projectWithUsers.duracion = project.duracion;
                projectWithUsers.imagen = _utilities.getImageOfProject(project.Id);
                projectWithUsers.descripcion = project.descripcion;
                projectWithUsers.archivado = project.archivado;
                projectWithUsers.infoCliente = project.infoCliente;

                for (int x = 0; x < users.Count; x++)
                {
                    var userIn = allUsers.Find(a => a.Id == users[x].idUsuario);
                    userIn.password = "No Password";
                    projectWithUsers.usuarios.Add(userIn);
                }
                
                if (projectWithUsers.archivado == true)
                {
                    withUsers.Add(projectWithUsers);
                }
            }
            

            return new OkObjectResult(withUsers);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2")]
        [HttpGet("getAllArchivedProjects")]
        public ActionResult<List<Project>> GetAllArchivedProjects()
        {
            var projects = _projectService.Get();
            List<ProjectWithUsers> archivedProjects = new List<ProjectWithUsers>();
            var allUsers = _userService.Get();

            if (projects.Count == 0)
            {
                return new NotFoundObjectResult("No existen proyectos");
            }

            var length = projects.Count;

            for (int i = 0; i < length; i++)
            {
                ProjectWithUsers projectWithUsers = new ProjectWithUsers();
                var users = _projectService.GetUsersFromProject(projects[i].Id);

                projectWithUsers.Id = projects[i].Id;
                projectWithUsers.nombre = projects[i].nombre;
                projectWithUsers.duracion = projects[i].duracion;
                projectWithUsers.imagen = _utilities.getImageOfProject(projects[i].Id);
                projectWithUsers.descripcion = projects[i].descripcion;
                projectWithUsers.archivado = projects[i].archivado;
                projectWithUsers.infoCliente = projects[i].infoCliente;

                for (int x = 0; x < users.Count; x++)
                {
                    var userIn = allUsers.Find(a => a.Id == users[x].idUsuario);
                    userIn.password = "No Password";
                    projectWithUsers.usuarios.Add(userIn);
                }

                if (projects[i].archivado == true)
                {
                    archivedProjects.Add(projectWithUsers);
                }
            }

            if (archivedProjects.Count == 0)
            {
                return new NotFoundObjectResult("No hay proyectos archivados");
            }

            return new OkObjectResult(archivedProjects);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpGet("nombre/{name}", Name = "GetProjectByName")]
        public ActionResult<List<Project>> Get(string name)
        {
            var projects = _projectService.GetByName(name);
            List<Project> notArchivedProjects = new List<Project>();

            if (projects.Count == 0)
            {
                return new NotFoundObjectResult("No existen coincidencias con: "+name);
            }

            var length = projects.Count;

            for(int i = 0; i < projects.Count; i++)
            {
                if (projects[i].archivado == false)
                {
                    notArchivedProjects.Add(projects[i]);
                }
            }

            if (notArchivedProjects.Count == 0)
            {
                return new NotFoundObjectResult("No existen proyectos activos que coincidan con: " + name);
            }

            return new OkObjectResult(notArchivedProjects);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2")]
        [HttpGet("{id}", Name = "GetProjectById")]
        public ActionResult<Project> Get(int id)
        {
            var project = _projectService.Get(id);

            if (project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: "+id);
            }

            project.imagen = _utilities.getImageOfProject(project.Id);

            return project;
        }
        
        [HttpGet("projectsByUser/{id}")]
        public ActionResult<List<Project>> GetProjectsByUser(int id)
        {
            var user = _userService.Get(id);

            if(user == null)
            {
                return new NotFoundObjectResult("No existe el usuario: "+id);
            }

            var projectsIds = _projectService.GetProjectsByUser(user.Id);

            if(projectsIds.Count == 0)
            {
                return new NotFoundObjectResult("El usuario no tiene proyectos asignados");
            }

            var count = projectsIds.Count;
            List<ProjectWithUsers> projectsWithUser = new List<ProjectWithUsers>();
            var allUsers = _userService.Get();

            for(int i=0; i<count; i++)
            {
                var idProyecto = projectsIds[i].idProyecto;
                var project = _projectService.Get(idProyecto);
                ProjectWithUsers projectWithUsers = new ProjectWithUsers();
                var users = _projectService.GetUsersFromProject(project.Id);

                projectWithUsers.Id = project.Id;
                projectWithUsers.nombre = project.nombre;
                projectWithUsers.duracion = project.duracion;
                projectWithUsers.imagen = _utilities.getImageOfProject(project.Id);
                projectWithUsers.descripcion = project.descripcion;
                projectWithUsers.archivado = project.archivado;
                projectWithUsers.infoCliente = project.infoCliente;

                for (int x = 0; x < users.Count; x++)
                {
                    var userIn = allUsers.Find(a => a.Id == users[x].idUsuario);
                    userIn.password = "No Password";
                    projectWithUsers.usuarios.Add(userIn);
                }

                if (project.archivado == false)
                {
                    projectsWithUser.Add(projectWithUsers);
                }
            }

            return new OkObjectResult(projectsWithUser);
        }
        
        [HttpGet("usersFromProject/{id}")]
        public ActionResult<List<Users>> GetUsersFromProject(int id)
        {
            var project = _projectService.Get(id);

            if(project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + id);
            }

            var usersId = _projectService.GetUsersFromProject(id);

            if (usersId.Count == 0)
            {
                return new NotFoundObjectResult("El proyecto " + id + ", no tiene usuarios asignados");
            }

            var count = usersId.Count;

            List<Users> users = new List<Users>();

            for(int i = 0; i < count; i++)
            {
                var userid = usersId[i].idUsuario;
                var user = _userService.Get(userid);
                user.password = "No password";
                users.Add(user);
            }

            return new OkObjectResult(users);

        }
        
        [HttpGet("getMenu/{idProyecto}")]
        public ActionResult<List<RecipeForMenu>> GetMenu(int idProyecto)
        {
            var project = _projectService.Get(idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + idProyecto);
            }

            var recipes = _recipeService.GetByProject(idProyecto);
            List<RecipeForMenu> menu = new List<RecipeForMenu>();

            if (recipes.Count == 0)
            {
                return new NotFoundObjectResult("Aun no hay recetas en el proyecto: " + idProyecto);
            }

            menu = _utilities.getMenu(idProyecto);

            return new OkObjectResult(menu);
        }

        [HttpGet("getBalancePoint/{idProyecto}")]
        public ActionResult<List<RecipeForMenu>> GetBalancePoint(int idProyecto)
        {
            var project = _projectService.Get(idProyecto);

            if (project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + idProyecto);
            }

            var recipes = _recipeService.GetByProject(idProyecto);
            List<RecipeForMenu> menu = new List<RecipeForMenu>();
            double meanTicket;
            double meanVariableCost;
            double grossProfit;
            double fixedCostTotal;
            double unitsRequired;
            double unitsIncome;
            double salesIncome;
            double variableCost;
            double netProfit;

            if (recipes.Count == 0)
            {
                return new NotFoundObjectResult("Aun no hay recetas en el proyecto: " + idProyecto);
            }

            menu = _utilities.getMenu(idProyecto);
            var recipesOfProject = _recipeService.GetByProject(idProyecto);

            double sumRecipes = 0;
            double sumDrinks = 0;
            double meanRecipePrice = 0;
            double meanDrinkPrice = 0;
            int recipesCount = 0;
            int drinksCount = 0;
            double meanMenuPrices = 0;
            double sumVariableRecipeCost = 0;
            double sumDrinksVariableCosts = 0;
            double meanVariableCosts = 0;
            double meanDrinksVariableCost = 0;
            double meanRecipesVariableCost = 0;

            for (int i = 0; i < menu.Count; i++)
            {
                var recipe = recipesOfProject.Find(Recipe => Recipe.Id == menu[i].idReceta);
                if(recipe.tipoReceta == 1)
                {
                    sumRecipes = sumRecipes + menu[i].precio;
                    sumVariableRecipeCost = sumVariableRecipeCost + recipe.costoTotal;
                    recipesCount = recipesCount + 1;
                }
                else if(recipe.tipoReceta == 3)
                {
                    sumDrinks = sumDrinks + menu[i].precio;
                    sumDrinksVariableCosts = sumDrinksVariableCosts + recipe.costoTotal;
                    drinksCount = drinksCount + 1;
                }
            }

            if (sumRecipes > 0)
            {
                meanRecipePrice = sumRecipes / recipesCount;
            }
            if (sumDrinks > 0)
            {
                meanDrinkPrice = sumDrinks / drinksCount;
            }

            meanMenuPrices = meanRecipePrice + meanDrinkPrice;

            if (sumDrinksVariableCosts > 0)
            {
                meanDrinksVariableCost = sumDrinksVariableCosts / drinksCount;
            }

            if(sumVariableRecipeCost > 0)
            {
                meanRecipesVariableCost = sumVariableRecipeCost / recipesCount;
            }

            meanVariableCosts = meanRecipesVariableCost + meanDrinksVariableCost;

            MeanCosts meanCostTable = new MeanCosts();

            //Primera Tabla
            meanTicket = meanMenuPrices;
            meanVariableCost = meanVariableCosts;
            grossProfit = meanTicket - meanVariableCost;
            fixedCostTotal = _utilities.GetSumFixedCosts(idProyecto);
            unitsRequired = fixedCostTotal / grossProfit;
            unitsIncome = unitsRequired * meanTicket;

            meanCostTable.ticketPromedio = meanTicket;
            meanCostTable.costoVariable = meanVariableCost;
            meanCostTable.utilidadBruta = grossProfit;
            meanCostTable.costosFijos = fixedCostTotal;
            meanCostTable.unidadesRequeridas = unitsRequired;
            meanCostTable.ingresoUnidades = unitsIncome;

            //Segunda tabla
            IncomeDistribuition incomeDistributionTable = new IncomeDistribuition();

            salesIncome = unitsRequired * meanTicket;
            //Aqui va el costo fijo total otra vez
            variableCost = meanVariableCost * unitsRequired;
            netProfit = Math.Round(salesIncome - fixedCostTotal - variableCost, 7);

            incomeDistributionTable.ingresoVenta = salesIncome;
            incomeDistributionTable.costosFijos = fixedCostTotal;
            incomeDistributionTable.costosVariables = variableCost;
            incomeDistributionTable.utilidadNeta = netProfit;

            //Tercera Tabla
            BalancePoint balancePointTable = new BalancePoint();

            for(int i = 0; i < 5; i++) {
                double unidadesComensales = 0;
                switch (i)
                {
                    case 0:
                        unidadesComensales = 0;
                        break;
                    case 1:
                        unidadesComensales = unitsRequired / 2;
                        break;
                    case 2:
                        unidadesComensales = unitsRequired;
                        break;
                    case 3:
                        unidadesComensales = unitsRequired * 1.5;
                        break;
                    case 4:
                        unidadesComensales = unitsRequired * 2;
                        break;
                }
                
                balancePointTable.precioVenta.Add(meanTicket);
                balancePointTable.unidades.Add(unidadesComensales);
                balancePointTable.ingresoTotal.Add(unidadesComensales * meanTicket);
                balancePointTable.costosFijos.Add(fixedCostTotal);
                balancePointTable.costoVariableUnitario.Add(meanVariableCost);
                balancePointTable.costoVariableTotal.Add(unidadesComensales * meanVariableCost);
                balancePointTable.costoTotal.Add(balancePointTable.costoVariableTotal[i] + fixedCostTotal);
                balancePointTable.utilidadNeta.Add(Math.Round(balancePointTable.ingresoTotal[i] - balancePointTable.costoTotal[i],7));
            }

            return new OkObjectResult(new {meanCostTable, incomeDistributionTable, balancePointTable });
        }

        [HttpGet("getImage/{id}")]
        public ActionResult<ProfilePicture> getProjectImage(int id)
        {
            var project = _projectService.Get(id);

            if(project == null)
            {
                return new NotFoundObjectResult("No se encontro el proyecto: " + id);
            }

            ProfilePicture image = new ProfilePicture();

            try
            {
                image.base64 = project.imagen;
            }
            catch (Exception e)
            {
                return new ConflictObjectResult("Ocurrio un error al obtener la imagen: " + e.ToString());
            }

            return new OkObjectResult(image);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2")]
        [HttpPost]
        public ActionResult<Project> Create(Project newProject)
        {
            newProject.archivado = false;
            Project project;
            
            try
            {
                project = _projectService.Create(newProject);
                Chat newChat = new Chat();
                newChat.idProyecto = project.Id;
                newChat.mensajes = new List<Mensaje>();
                _chatService.Create(newChat);

            } catch (Exception)
            {
                return new ConflictResult();
            }

            return new CreatedAtRouteResult("GetProjectById", new { id = newProject.Id.ToString() }, newProject);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2")]
        [HttpPost("userToProject")]
        public ActionResult<UserToProject> AddUserToProject(UsersToProject userToProject)
        {
            var project = _projectService.Get(userToProject.idProyecto);

            if (project == null)
            {
                return new NotFoundObjectResult("Proyecto no encontrado: " + userToProject.idProyecto);
            }

            if(project.archivado == true)
            {
                return new NotFoundObjectResult("No hay proyectos activos con el id: "+userToProject.idProyecto);
            }

            var count = userToProject.usuarios.Length;

            for (int i = 0; i < count; i++)
            {
                var user = _userService.Get(userToProject.usuarios[i]);
                var userInProject = _projectService.GetUserInProject(userToProject.usuarios[i], userToProject.idProyecto);

                if(userInProject != null)
                {
                    return new ConflictObjectResult(userInProject);
                }

                if(user == null)
                {
                    return new NotFoundObjectResult("Usuario no encontrado: " + userToProject.usuarios[i]);
                }
            }

            for (int i=0; i < count; i++)
            {
                _projectService.AddUserToProject(userToProject.idProyecto, userToProject.usuarios[i]);
            }

            
            return new OkObjectResult(userToProject);

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2")]
        [HttpPut("{id}")]
        public IActionResult Update(int id, Project newProject)
        {
            var oldProject = _projectService.Get(id);

            if(oldProject == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: "+id);
            }

            if(oldProject.archivado == true)
            {
                return new NotFoundObjectResult("No existen proyectos activos con el id: " + id);
            }

            newProject.Id = id;
            _projectService.Update(id, newProject);

            return new NoContentResult();
        }

        [HttpPut("updateImage/{id}")]
        public async Task<ActionResult> updateImage(int id, ProfilePicture image)
        {
            var project = _projectService.Get(id);

            if (project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + id);
            }

            if (image.base64 == null)
            {
                return new ConflictObjectResult("Formato de imagen no valido");
            }

            try
            {
                var fileKey = $"projectimg_{project.Id}.png";
                var fileTransferUtility = new TransferUtility(_s3Client);

                using (var stream = new MemoryStream(Convert.FromBase64String(image.base64)))
                {
                    await fileTransferUtility.UploadAsync(stream, _bucketName, fileKey);
                }

                var fileUrl = _s3Client.GetPreSignedURL(new GetPreSignedUrlRequest
                {
                    BucketName = _bucketName,
                    Key = fileKey,
                    Expires = DateTime.Now.AddYears(1)
                });

                project.imagen = fileUrl;

                _projectService.Update(id, project);
            }
            catch (Exception ex)
            {
                return new ConflictObjectResult("Ocurrio un error al guardar la imagen: " + ex.Message);
            }

            return new OkObjectResult(project.imagen);
        }



        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("updateTemplate/{id}")]
        [DisableRequestSizeLimit]
        public async Task<ActionResult> updateTemplateImg(int id, ProfilePicture image)
        {
            var project = _projectService.Get(id);

            if (project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + id);
            }

            if (image.base64 == "")
            {
                return new NotFoundObjectResult("La imagen no tiene un formato valido o esta vacia");
            }

            try
            {
                var fileKey = $"templatePdf_{id}.png";
                var fileTransferUtility = new TransferUtility(_s3Client);

                using (var stream = new MemoryStream(Convert.FromBase64String(image.base64)))
                {   
                    await fileTransferUtility.UploadAsync(stream, _bucketName, fileKey);
                }

                var fileUrl = _s3Client.GetPreSignedURL(new GetPreSignedUrlRequest
                {
                    BucketName = _bucketName,
                    Key = fileKey,
                    Expires = DateTime.Now.AddYears(1)
                });

                project.imagen = fileUrl;

                _projectService.Update(id, project);
            }
            catch (Exception ex)
            {
                return new ConflictObjectResult("Ocurrio un error al subir la imagen: " + ex.Message);
            }

            return new OkObjectResult("Imagen actualizada");
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpGet("getTemplate/{id}")]
        public ActionResult getTemplateImg(int id)
        {
            var project = _projectService.Get(id);

            if (project == null)
            {
                return new NotFoundObjectResult("No se encontro el proyecto: " + id);
            }

            ProfilePicture picture = new ProfilePicture();

            try
            {
                var filePath = "/var/img/templatePdf_" + id + ".png";

                if (File.Exists(filePath))
                {
                    Byte[] bytes = File.ReadAllBytes(filePath);

                    picture.base64 = Convert.ToBase64String(bytes);
                }
                else
                {
                    Byte[] bytes = File.ReadAllBytes("/var/img/defaultTemplate.png");
                    picture.base64 = Convert.ToBase64String(bytes);
                }

            }
            catch (Exception e)
            {
                return new ConflictObjectResult("Ocurrio un error al obtener la imagen: " + e.ToString());
            }

            return new OkObjectResult(picture);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpPost("delete")]
        public IActionResult Delete(DeleteProject infoDelete)
        {
            var id = infoDelete.idProyecto;
            var idUser = infoDelete.idUsuario;

            var user = _userService.Get(idUser);
            var project = _projectService.Get(id);

            if(project == null)
            {
                return new NotFoundResult();
            }

            if(user == null)
            {
                return new NotFoundObjectResult("El usuario " + idUser + ", no existe");
            }

            if(user.password != infoDelete.password)
            {
                return new ConflictObjectResult("La contraseña ingresada es incorrecta");
            }

            //Elimino el proyecto
            _projectService.Remove(id);
            //Elimino la relacion entre usuarios y proyectos
            _projectService.RemoveProjects(id);
            //Elimino ingredientes del proyecto
            _ingredientService.RemoveIngredientsFromProject(id);
            //Elimino Proveedores del proyecto
            _supplierService.RemoveSuppliersFromProject(id);
            //Elimino costos fijos del proyecto
            _fixedCostService.RemoveFixedCostFromProject(id);
            //Elimino recetas del proyecto
            _recipeService.RemoveRecipesFromProject(id);
            //Elimino costos de servicios y su relacion con insumos
            _serviceCostService.RemoveServiceCostsOfProject(id);
            //Elimino las ganancias del proyecto
            _profitService.RemoveAllFromProject(id);
            //Elimino insumos del proyecto
            _consumableService.RemoveConsumablesFromProject(id);
            //Elimino SubRecetas del proyecto
            _subRecipeService.RemoveSubRecipesFromProject(id);
            //Elimino Chat del proyecto
            _chatService.RemoveByProject(id);

            var filePath = "/var/img/projectimg_" + project.Id + ".png";
            File.Delete(filePath);

            return new NoContentResult();
        }

        [HttpGet("resumeProject/{id}")]
        public ActionResult<Project> resumeProject(int id)
        {
            var project = _projectService.Get(id);

            if (project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + id);
            }

            if (project.archivado != true)
            {
                return new ConflictObjectResult("El proyecto esta activo actualmente");
            }

            project.archivado = false;

            _projectService.Update(project.Id, project);

            return new OkObjectResult(project);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2")]
        [HttpDelete("archiveProject/{id}")]
        public ActionResult<Project> archiveProject(int id)
        {
            var project = _projectService.Get(id);

            if(project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto: " + id);
            }

            if(project.archivado == true)
            {
                return new ConflictObjectResult("El proyecto ya esta archivado");
            }

            project.archivado = true;

            _projectService.Update(project.Id, project);

            return new OkObjectResult(project);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2")]
        [HttpDelete("removeFromProject/{idProyecto}/{idUsuario}")]
        public IActionResult DeleteUserFromProject(int idProyecto, int idUsuario)
        {
            var project = _projectService.Get(idProyecto);
            var user = _userService.Get(idUsuario);

            if (project == null || user == null)
            {
                return new NotFoundResult();
            }

            if(project.archivado == true)
            {
                return new NotFoundObjectResult("No existen proyectos activos con el id: " + idProyecto);
            }

            UserToProject userToProjectOut = new UserToProject();
            userToProjectOut.idProyecto = idProyecto;
            userToProjectOut.idUsuario = idUsuario;

            _projectService.RemoveOneUserFromProject(userToProjectOut);
            return new NoContentResult();
        }

    }
}
