﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class IngredientsController
    {
        private readonly IngredientService _ingredientService;
        private readonly ProjectService _projectService;
        private readonly RecipeService _recipeService;
        private readonly Utilities _utilities;

        public IngredientsController(IngredientService ingredientService, ProjectService projectService, RecipeService recipeService, Utilities utilities)
        {
            _ingredientService = ingredientService;
            _projectService = projectService;
            _recipeService = recipeService;
            _utilities = utilities;
        }

        [HttpGet]
        public ActionResult<List<Ingredient>> Get() =>
            _ingredientService.Get();

        [HttpGet("{id}", Name = "GetIngredient")]
        public ActionResult<Ingredient> Get(int id)
        {
            var ingredient = _ingredientService.Get(id);

            if (ingredient == null)
            {
                return new NotFoundResult();
            }

            return ingredient;
        }

        [HttpGet("searchByProject/{id}")]
        public ActionResult<List<Ingredient>> GetByProjectId(int id)
        {
            var ingredients = _ingredientService.GetByProjectId(id);

            if(ingredients.Count == 0)
            {
                return new NotFoundResult();
            }

            return ingredients;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost("nombre", Name = "GetIngredientByName")]
        public ActionResult<List<Ingredient>> Get(SearchIngredientName ingredientIn)
        {
            var ingredient = _ingredientService.GetByName(ingredientIn.nombre, ingredientIn.idProyecto);

            if (ingredient.Count == 0)
            {
                return new NotFoundResult();
            }

            return ingredient;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost]
        public ActionResult<Ingredient> Create(Ingredient ingredient)
        {
            var project = _projectService.Get(ingredient.idProyecto);
            var oldIngredient = _ingredientService.GetByName(ingredient.nombre, ingredient.idProyecto);

            if (project == null)
            {
                return new NotFoundObjectResult("No existe el proyecto "+ingredient.idProyecto);
            }

            if(oldIngredient.Count > 0)
            {
                return new ConflictObjectResult("El ingrediente " + ingredient.nombre + ", ya existe en este proyecto");
            }

            double primeraParte = ingredient.precioPresentacion / ingredient.presentacion;
            double parteIntermedia = Convert.ToDouble(ingredient.merma) / 100;
            double segundaParte = 1 - parteIntermedia;
            double unitario = primeraParte / segundaParte;

            ingredient.precioUnitario = unitario;

            _ingredientService.Create(ingredient);

            return new CreatedAtRouteResult("GetIngredient", new { id = ingredient.Id.ToString() }, ingredient);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("{id}")]
        public IActionResult Update(int id, Ingredient ingredientIn)
        {
            var project = _projectService.Get(ingredientIn.idProyecto);

            if (project == null)
            {
                return new NotFoundResult();
            }

            var oldIngredient = _ingredientService.Get(id);

            if (oldIngredient == null)
            {
                return new NotFoundResult();
            }

            ingredientIn.Id = id;
            double primeraParte = ingredientIn.precioPresentacion / ingredientIn.presentacion;
            double parteIntermedia = Convert.ToDouble(ingredientIn.merma) / 100;
            double segundaParte = 1 - parteIntermedia;
            double unitario = primeraParte / segundaParte;
            ingredientIn.precioUnitario = unitario;
            _ingredientService.Update(id, ingredientIn);

            //Actualizo costos de recetas
            _utilities.UpdateRecipesCosts(ingredientIn);
            //Actualizo costos de subRecetas
            _utilities.UpdateSubRecipesCosts(ingredientIn);

            return new NoContentResult();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var ingredient = _ingredientService.Get(id);

            if (ingredient == null)
            {
                return new NotFoundResult();
            }

            //Elimino ingredientes de recetas y subrecetas y actualizo los costos
            _utilities.RemoveIngredients(ingredient);

            _ingredientService.Remove(ingredient.Id);

            return new NoContentResult();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1")]
        [HttpGet("arreglarIngredientes")]
        public ActionResult arreglaIngredientes()
        {
            _utilities.arreglarCostosIngredientes();
            return new OkObjectResult("Se arreglaron");
        }
    }
}
