﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1, 2, 3, 4")]
    public class ProfitsController
    {
        private readonly ProjectService _projectService;
        private readonly ProfitService _profitService;

        public ProfitsController(ProjectService projectService, ProfitService profitService)
        {
            _projectService = projectService;
            _profitService = profitService;
        }

        [HttpGet]
        public ActionResult<List<Profit>> Get() =>
            _profitService.Get();

        [HttpGet("{id}")]
        public ActionResult<Profit> Get(int id)
        {
            var profit = _profitService.Get(id);

            if(profit == null)
            {
                return new NotFoundObjectResult("No existe la ganancia: " + id);
            }

            return new OkObjectResult(profit);
        }

        [HttpGet("fromProject/{idProyecto}")]
        public ActionResult<List<Profit>> GetByProject(int idProyecto)
        {
            var profits = _profitService.GetByProject(idProyecto);
            var project = _projectService.Get(idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto: " + idProyecto + ", no existe");
            }

            if(profits.Count == 0)
            {
                return new NotFoundObjectResult("El proyecto no tiene asignada ninguna ganancia");
            }

            return new OkObjectResult(profits);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPost(Name = "CreateProfit")]
        public ActionResult<Profit> Create(Profit newProfit)
        {
            var oldProfit = _profitService.GetByDate(newProfit.fecha, newProfit.idProyecto);
            var project = _projectService.Get(newProfit.idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto: " + newProfit.idProyecto + ", no existe");
            }

            if (oldProfit.Count > 0)
            {
                return new ConflictObjectResult("Ya hay una ganancia con la fecha: " + newProfit.fecha + ", en este proyecto");
            }

            var created = _profitService.Create(newProfit);

            return new CreatedAtRouteResult("CreateProfit", new { Id = created.Id.ToString() }, created);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpPut("{id}")]
        public ActionResult<Profit> Update(int id, Profit newProfit)
        {
            var oldProfit = _profitService.Get(id);
            var project = _projectService.Get(newProfit.idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto: " + newProfit.idProyecto + ", no exite");
            }

            if(oldProfit == null)
            {
                return new NotFoundObjectResult("La ganancia: " + id + ", la cual intentas modificar, no existe");
            }

            newProfit.Id = oldProfit.Id;
            _profitService.Update(id, newProfit);

            return new OkObjectResult(newProfit);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("{id}")]
        public ActionResult Remove(int id)
        {
            var profit = _profitService.Get(id);

            if(profit == null)
            {
                return new NotFoundObjectResult("La ganancia: " + id + ", no existe");
            }

            _profitService.Remove(id);

            return new OkResult();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "1,2,3")]
        [HttpDelete("allFromProject/{idProyecto}")]
        public ActionResult RemoveFromProject(int idProyecto)
        {
            var project = _projectService.Get(idProyecto);
            var profits = _profitService.GetByProject(idProyecto);

            if(project == null)
            {
                return new NotFoundObjectResult("El proyecto: " + idProyecto + ", no existe");
            }

            if(profits.Count == 0)
            {
                return new NotFoundObjectResult("No existen ganancias asignadas a este proyecto");
            }

            _profitService.RemoveAllFromProject(idProyecto);
            return new OkResult();
        }
    }
}
