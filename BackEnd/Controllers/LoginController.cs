﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GProGApi.Models;
using GProGApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace GProGApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController
    {
        private readonly UserService _userService;
        private readonly IConfiguration _configuration;

        public LoginController(UserService userService, IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }

        [HttpGet("archivo")]
        public ActionResult guardarArchivo()
        {
            string path = "/var/img/prueba1.txt";
            string createText = "Hello and Welcome 2" + Environment.NewLine;
            File.WriteAllText(path, createText);
            return new OkObjectResult("Jalo");
        }

        [HttpPost]
        public IActionResult Login(UserLogin userLog)
        {
            var user = _userService.GetByEmail(userLog.correo);

            if (user == null)
            {
                return new NotFoundObjectResult("Ocurrio un error al intentar acceder al sistema, verificar que el usuario sea correcto.");
            }

            if (user.password != userLog.password)
            {
                return new ConflictObjectResult("Ocurrio un error al intentar acceder al sistema, favor de verificar que la contraseña sea correcta.");
            }

            return BuildToken(userLog, user);
        }

        private IActionResult BuildToken(UserLogin userLog , Users user)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, userLog.correo),
                new Claim("LogueadoConPassword",userLog.password),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, user.tipousuario.ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["LLAVE_SECRETA"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddHours(12);
            //var expiration = DateTime.UtcNow.AddMinutes(30);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: "saconsultoria.com",
                audience: "saconsultoria.com",
                claims: claims,
                expires: expiration,
                signingCredentials: creds
                );

            return new OkObjectResult(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                user = user,
                expiration = expiration
            });

        }
    }
}
