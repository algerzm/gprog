import Vue from "vue";
import Vuex from "vuex";
import i18n from '../i18n';
//import notification from'../WebSocket/notification';
import api from '../api/api';
Vue.use(Vuex);
 //El store es una memoria compartida por todos los componentes que se montan en la aplicacion,
 //sirve para almacenar informacion importante para el funcionamiento del sistema, cuenta con states (las variables),
 //getters (los metodos para obtener su valor actual), mutations(los metodos para asignarles valores o setters),
 //y actions (no las usamos de momento)




export default new Vuex.Store({
    strict: false,
    state:{ 
        user: JSON.parse(localStorage.getItem('user')),  //guarda el ojeto con los datos del usuario
        logged: JSON.parse(localStorage.getItem('logged')),         //guarda si el usuario esta loggeado
        token: localStorage.getItem('token'),           //guarda el token
        currentproject: JSON.parse(localStorage.getItem('Cproject')),  //guarda el proyecto actual
        useful: '',   //variable para pasar datos necesarios entre componentes sin usar props
        notifications: JSON.parse(localStorage.getItem('notifications')),
        actmenu: [{id: 1, name: i18n.t('menu.projects'), route: 'projects', active: true, disabled: false,icon:"fas fa-edit"},
                    {id: 2, name: i18n.t('menu.archived'), route: 'archived', active: false, disabled: false,icon:"far fa-folder-open"},
                    {id: 3, name: i18n.t('menu.manage'), route: 'manage', active: false, disabled: false,icon:"fas fa-toolbox"},
                ], 
        actsideP: [{id: 1,name: 'Info', route:'projectinfo', active: false, disabled: false,icon:"fas fa-info-circle"},
                {id: 2,name: 'Analysis', route: 'projectanalysis',active: true, disabled: false,icon:"fas fa-search-dollar"},
                {id: 3,name: 'Management', route:'projectmanagement',active: false, disabled: false,icon:"fas fa-file-invoice-dollar"},
                {id: 4,name: 'Content', route: 'projectview',active: true, disabled: false,icon:"fas fa-table"},
                
                {id: 5,name: 'Tools', route:'projecttools',active: false, disabled: false,icon:"fas fa-tools"},
                {id: 6,name: 'Feedback', route:'projectfeedback',active: false, disabled: false,icon:"fas fa-comments"},
                {id: 7,name: 'Settings', route:'projectsettings',active: false, disabled: false,icon:"fas fa-sliders-h"}
            ],
        actsideA: [{id: 1,name: 'General', route:'account',active: false, disabled: false,icon:"fas fa-address-card"},
                {id: 2,name: 'Customization', route:'preferences',active: false, disabled: false,icon:"fas fa-palette"},
                {id: 3,name: 'Stats', route:'stats',active: false, disabled: false,icon:"far fa-chart-bar"}
            ],
        units: JSON.parse(localStorage.getItem('units')),
        types: JSON.parse(localStorage.getItem('types')),
    },
    getters: { 
        units: state =>{
            return state.units
        },
        types: state =>{
            return state.types
        },
        user: state => {
            return state.user
          },
         logged: state => {
            return state.logged
          },
          username: state=>{
                return state.user.nombre
          },
          userpass: state=>{
            return state.user.password
        },
          userlvl: state =>{
              return state.user.tipousuario
          },
          useremail: state=>{
              return state.user.correo
          },
          userid: state=>{
            //console.log(state.user.id)
            return state.user.id
        },
          token: state => {
            return state.token
          },
          currentProject: state => {
            return state.currentproject
          },
          actmenu:  state=>{
            state.actmenu[0].name=i18n.t('menu.projects');
            state.actmenu[1].name=i18n.t('menu.archived');
            state.actmenu[2].name=i18n.t('menu.manage');
            return state.actmenu
          },
          actsideP: state=>{
            state.actsideP[0].name=i18n.t('sidemenuProject.opt1');
            state.actsideP[1].name=i18n.t('sidemenuProject.opt2');
            state.actsideP[2].name=i18n.t('sidemenuProject.opt4');
            state.actsideP[3].name=i18n.t('sidemenuProject.opt3');
            state.actsideP[4].name=i18n.t('sidemenuProject.opt5');
            state.actsideP[5].name=i18n.t('sidemenuProject.opt6');
            state.actsideP[6].name=i18n.t('sidemenuProject.opt7');
            return state.actsideP
          },
          actsideA: state=>{
            state.actsideA[0].name=i18n.t('sidemenuUser.opt1');
            state.actsideA[1].name=i18n.t('sidemenuUser.opt2');
            state.actsideA[2].name=i18n.t('sidemenuUser.opt3');
            return state.actsideA
          },
          useful: state=>{
            return state.useful
          },
          notifications:  state=>{
            return state.notifications
          }
    },
    mutations: {
        setTypes(state,change){
            state.types=change;
            localStorage.setItem("types",JSON.stringify(state.types));
        },
        setUnits(state,change){
            state.units=change;
            localStorage.setItem("units",JSON.stringify(state.units));
        },
        setNoti(state,change){

            state.notifications=change;
            localStorage.setItem("notifications",JSON.stringify(state.notifications));
        },
        addNoti(state,change){ //modificar implementacion de este metodo 
            var noti={
                id: change.id,
                titulo: change.title,
                mensaje: change.message
            }
            state.notifications.push(noti);
            localStorage.setItem("notifications",JSON.stringify(state.notifications));
        },
        delNoti(state,change){
            const index = state.notifications.indexOf(change);
            if (index > -1) {
                state.notifications.splice(index, 1);
            }
            localStorage.setItem("notifications",JSON.stringify(state.notifications));
        },
        setUseful(state,change){
            state.useful=change;
        },
        setUser(state, change){
            state.user= change;
            localStorage.setItem("user",JSON.stringify(change));
            console.log(JSON.parse(localStorage.getItem('user')));
        },
        setPassword(state,change){
            state.user.password=change;
            localStorage.setItem("user",JSON.stringify(state.user));
        },
        setLogged(state, change){
            state.logged= change;
            localStorage.setItem("logged",change);
        }
        ,
        setToken(state, change){
            state.token= change;
            localStorage.setItem('token',change);
        },
        setCurrProject(state, change){
            state.currentproject= change;
            localStorage.setItem('Cproject',JSON.stringify(change));
            console.log(JSON.parse(localStorage.getItem('Cproject')));
            //console.log(localStorage.getItem('Cproject'));
        },
        setMenu(state, change){

            state.actmenu[change.id-1]= change;
            if(change.active){
                for(var i=0;i<state.actmenu.length;i++){
                    if(state.actmenu[i]!=change){
                        state.actmenu[i].active=false;
                    }
                }
            }
        },
        setSideP(state, change){
            state.actsideP[change.id-1]= change;
            if(change.active){
                for(var i=0;i<state.actsideP.length;i++){
                    if(state.actsideP[i]!=change){
                        state.actsideP[i].active=false;
                    }
                }
            }
        },
        setSideA(state, change){
            state.actsideA[change.id-1]= change;
            if(change.active){
                for(var i=0;i<state.actsideA.length;i++){
                    if(state.actsideA[i]!=change){
                        state.actsideA[i].active=false;
                    }
                }
            }
        },
    },
    actions:{
        async delNoti(context,change){
            const token= 'Bearer '+this.state.token
            const url='/notifications/notificationReaded';
            const headers={'headers':
                        {
                            'Content-Type': 'application/json',
                            'Authorization': token
                            
                        }};
            await api.post(url,
                {
                    idNotificacion: change.id,
                    idUsuario: this.state.user.id,

                },headers).then((response) => 
                {   
                    console.log(response);
                    context.commit('delNoti',change);
                }, (error) => {
                    console.log(error);
                    if(error.response.status === 404){
                        context.commit('delNoti',change);
                    }
                });            
        },

    }
})