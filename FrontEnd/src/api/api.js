import axios from 'axios';
import store from '../store';
import helper from './helper';
var api = axios.create({
  // baseURL: 'https://gapi.montza.com/api',
  baseURL: 'http://localhost:4005/api',
});

async function verifyToken() {
  const token = 'Bearer ' + store.getters.token;
  const url = '/users/validate';
  const headers = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: token,
    },
  };
  console.log('Entro a la consulta');
  /* eslint-disable no-unused-vars */
  const resp = await api.get(url, headers).then(
    (response) => {
      console.log('DESPUES DE VERIFICAR EL TOKEN');
      return true;
    },
    (error) => {
      return false;
    }
  );
  console.log('Valor de resp');
  console.log(resp);
  return resp;
  /* eslint-enable no-unused-vars */
}

api.interceptors.response.use(
  (response) => {
    return response;
  },
  async (err) => {
    const {
      config,
      response: { data, status },
    } = err;

    console.log(config);
    console.log(data);
    console.log(status);

    if (config.url == '/users/validate') {
      return Promise.reject(err);
    }
    if (status == 401) {
      console.log(await verifyToken());
      if ((await verifyToken()) == true) {
        console.log('No tienes permisos para realizar esta accion!');
      } else {
        console.log('Mostrar modal');
        console.log(helper.obj);
        helper.show();
      }
    }

    return Promise.reject(err);
  }
);

api.prototype.open = function () {
  if (this.obj != null) {
    console.log(this.obj);
    this.obj.$bvModal.show('modal-a1');
  }
};

api.prototype.setObj = function (obj1) {
  this.obj = obj1;
  console.log('SE ASIGNO THIS');
};
api.obj = null;
//baseURL: 'http://70.35.202.215:4005/api'

export default api;
