import Vue from "vue";
import Router from "vue-router";
import i18n from '../i18n'
import store from "../store"; 
import chatSocket from'../WebSocket/Chat';

import Login from "../components/Login";
import Projects from "../components/Projects";
import Archived from "../components/Archived";
import Manage from "../components/Manage";
import AccountG from "../components/AccountGeneral";
import AccountP from "../components/AccountPreferences";
import AccountStats from "../components/AccountStats";
import ProjectV from "../components/ProjectView";
import ProjectSettings from "../components/ProjectSettings";
import UserOptions from "../components/UserOptions";
import ProjectOptions from "../components/ProjectOptions";
import ProjectInfo from "../components/ProjectInfo";
import ProjectManagement from "../components/ProjectManagement";
import ProjectFeedback from "../components/ProjectFeedback";
import NewRecipe from "../components/NewRecipe"
import SystemOptions from "../components/SystemOptions"
import Tools from "../components/Tools"
import NewSubRecipe from "../components/NewSubRecipe"
import Menu from "../components/Menu";
import ProjectAnalysis from "../components/ProjectAnalysis";
import BalancePoint from "../components/BalancePoint";
import Aboutus from "../components/Aboutus";
import Categories from "../components/Categories";

Vue.use(Router);

const myrouter = new Router({
    mode: 'history',
    //hash: false,
    routes: [
            {
                path: '*',
                redirect: '/en/login'
            },
            {
            path: '/:lang',
            component: {
                render (c) { return c('router-view')}
            },
            children: [
                { 
                    name: 'aboutus',
                    path: "about_us",
                    component: Aboutus,
                    meta: { reqAuth: true},
                },
                { 
                    name: 'login',
                    path: "login",
                    component: Login,
                    meta: { reqAuth: false},
                },
                { 
                    name: 'projects',
                    path: "projects", 
                    component: Projects,
                    meta: { reqAuth: true}
                },
                { 
                    name: 'archived',
                    path: "archived", 
                    component: Archived,
                    meta: { reqAuth: true}
                }, //Rutas dentro de un proyecto
                {
                    name: 'projectview',
                    path: "project/view", 
                    component: ProjectV,
                    meta: { reqAuth: true}
                },

                { 
                    name: 'projectinfo',
                    path: "project/info", 
                    component: ProjectInfo,
                    meta: { reqAuth: true}
                },
                { 
                    name: 'projectmanagement',
                    path: "project/management", 
                    component: ProjectManagement,
                    meta: { reqAuth: true}
                },
                { 
                    name: 'projectfeedback',
                    path: "project/feedback", 
                    component: ProjectFeedback,
                    meta: { reqAuth: true},
                    beforeEnter:  async (to, from, next) => {
                        if(chatSocket.connectionState != signalR.HubConnectionState.Connected){
                            console.log("ENTRO a conectarlo")
                                await chatSocket.start().catch(function(err) {
                                    return console.error(err.toString());
                                });
                            chatSocket.invoke("JoinRoom", store.getters.currentProject.id)
                            .catch(function(err) {
                                return console.error(err.toString());
                            });
                        }
                        next();
                    }

        
                },
                {
                    name: "projectnew_recipe", 
                    path: "project/new_recipe", 
                    component: NewRecipe,
                    meta: { reqAuth: true}
                },
                {
                    name: "projectnew_subrecipe", 
                    path: "project/new_subrecipe", 
                    component: NewSubRecipe,
                    meta: { reqAuth: true}
                },
                { 
                    name: "projectsettings",
                    path: "project/settings", 
                    component: ProjectSettings,
                    meta: { reqAuth: true}
                },
                { 
                    name: "projecttools",
                    path: "project/tools", 
                    component: Tools,
                    meta: { reqAuth: true}
                },
                { 
                    name: "projectbalance",
                    path: "project/balance", 
                    component: BalancePoint,
                    meta: { reqAuth: true}
                },
                { 
                    name: "projectanalysis",
                    path: "project/analysis", 
                    component: ProjectAnalysis,
                    meta: { reqAuth: true}
                },
                { 
                    name: 'projectmenu',
                    path: "project/tools/menu", 
                    component: Menu,
                    meta: { reqAuth: true}
                },//Rutas de usuario
                { 
                    name: 'stats',
                    path: "user/stats", 
                    component: AccountStats,
                    meta: { reqAuth: true}
                },
                { 
                    name: 'preferences',
                    path: "user/preferences", 
                    component: AccountP,
                    meta: { reqAuth: true}
                },
                { 
                    name: 'account',
                    path: "user/general", 
                    component: AccountG,
                    meta: { reqAuth: true}
                },//Rutas dentro de gestion/manage
                { 
                    name: 'manage',
                    path: "manage", 
                    component: Manage,
                    meta: { reqAuth: true}
                },
                { 
                    name: 'useroptions',
                    path: "manage/users", 
                    component: UserOptions,
                    meta: { reqAuth: true}
                },
                { 
                    name: 'projectoptions',
                    path: "manage/projects", 
                    component: ProjectOptions,
                    meta: { reqAuth: true}
                },
                { 
                    name: 'systemoptions',
                    path: "manage/system", 
                    component: SystemOptions,
                    meta: { reqAuth: true}
                },  
                { 
                    name: 'projectcategories',
                    path: "manage/system/categories", 
                    component: Categories,
                    meta: { reqAuth: true}
                },   
            ]          
        }
    ]
});

const signalR = require('@aspnet/signalr');



myrouter.beforeEach((to,from,next)=>{
    var language= to.params.lang;
    //console.log(language);
    const locales=i18n.availableLocales;
    //console.log(locales);
    if(language ==locales[0]){
        i18n.locale=language;
    }
    else if(language ==locales[1]){
        i18n.locale=language;
    }
    else{
        language=locales[0];
        i18n.locale=language;
    }

    if (to.matched.some(record => record.meta.reqAuth)) { //Revisa si la ruta destino requiere estar loggeado

        if (store.getters.logged != true ) // si no esta loggeado lo manda al login
        {
            next({ name: 'login', params: {lang: i18n.locale} });
        } 
        else 
        {
            switch (to.name) {//SECCION DE VALIDACIONES PARA EL MENU
                case 'projects':
                    store.commit("setMenu",{id: 1, name: i18n.t('menu.projects'), route: 'projects', active: true, disabled: false,icon:"fas fa-edit"})
                    next();
                    break;
                case 'manage':
                    if(store.getters.user.tipousuario == 1){
                        store.commit("setMenu", {id: 3, name: i18n.t('menu.manage'), route: 'manage', active: true, disabled: false,icon:"fas fa-toolbox"})
                        next();
                    }
                    else{
                        next({ name: 'projects', params: {lang: i18n.locale} });
                    }
                    break;
                case 'systemoptions':
                    if(store.getters.user.tipousuario == 1){
                        next();
                    }
                    else{
                        next({ name: 'projects', params: {lang: i18n.locale} });
                    }
                    break;
                case 'useroptions':
                    if(store.getters.user.tipousuario == 1){
                        next();
                    }
                    else{
                        next({ name: 'projects', params: {lang: i18n.locale} });
                    }
                    break;
                case 'projectcategories':
                    if(store.getters.user.tipousuario == 1){
                        next();
                    }
                    else{
                        next({ name: 'projects', params: {lang: i18n.locale} });
                    }
                    break;
                case 'projectoptions':
                    if(store.getters.user.tipousuario == 1){
                        next();
                    }
                    else{
                        next({ name: 'projects', params: {lang: i18n.locale} });
                    }
                    break;
                case 'archived':
                        store.commit("setMenu",{id: 2, name: i18n.t('menu.archived'), route: 'archived', active: true, disabled: false,icon:"far fa-folder-open"})
                        next();
                    break;
                case 'projectinfo':  //SECCION DE VALIDACIONES PARA EL MENU DE PROYECTOS
                    store.commit("setSideP", {id: 1,name: i18n.t('sidemenuProject.opt1'), route: 'projectinfo', active: true, disabled: false,icon:"fas fa-info-circle"})
                    next();
                    break;
                case 'projectanalysis':
                    store.commit("setSideP", {id: 2,name: i18n.t('sidemenuProject.opt2'), route: 'projectanalysis' ,active: true, disabled: false,icon:"fas fa-search-dollar"})
                    next();
                    break;
                case 'projectmanagement':
                    store.commit("setSideP",  {id: 3,name: i18n.t('sidemenuProject.opt4'), route:'projectmanagement',active: true, disabled: false,icon:"fas fa-file-invoice-dollar"})
                    next();
                    break;
                case 'projectview':
                    store.commit("setSideP", {id: 4,name: i18n.t('sidemenuProject.opt3'), route: 'projectview' ,active: true, disabled: false,icon:"fas fa-table"})
                    next();
                    break;
                case 'projectnew_recipe':
                case 'projectnew_subrecipe':
                    store.commit("setSideP", {id: 4,name: i18n.t('sidemenuProject.opt3'), route: 'projectview' ,active: true, disabled: false,icon:"fas fa-table"})
                    next();
                    break;

                case 'projecttools':
                    store.commit("setSideP",{id: 5,name: i18n.t('sidemenuProject.opt5'), route:'projecttools',active: true, disabled: false,icon:"fas fa-tools"}) 
                    next();
                    break;
                case 'projectmenu':
                case 'projectbalance':
                    store.commit("setSideP",{id: 5,name: i18n.t('sidemenuProject.opt5'), route:'projecttools',active: true, disabled: false,icon:"fas fa-tools"}) 
                    next();
                    break;
                case 'projectfeedback':
                    store.commit("setSideP",  {id: 6,name: i18n.t('sidemenuProject.opt6'), route:'projectfeedback',active: true, disabled: false,icon:"fas fa-comments"})
                    next();
                    break;
                case 'projectsettings':
                    if(store.getters.user.tipousuario < 3){
                        store.commit("setSideP",  {id: 7,name: i18n.t('sidemenuProject.opt7'), route:'projectsettings',active: true, disabled: false,icon:"fas fa-sliders-h"})
                        next(); 
                    }
                    else{
                        next({ name: 'projectview', params: {lang: i18n.locale} });
                    }
                    break;
                case 'account'://SECCION DE VALIDACIONES PARA EL MENU DE USUARIO
                    store.commit("setSideA",{id: 1,name: i18n.t('sidemenuUser.opt1'), route:'account',active: true, disabled: false,icon:"fas fa-address-card"})
                    next();
                    break;
                case 'preferences'://SECCION DE VALIDACIONES PARA EL MENU DE USUARIO
                    store.commit("setSideA",{id: 2,name: i18n.t('sidemenuUser.opt2'), route:'preferences',active: true, disabled: false,icon:"fas fa-palette"})
                    next();
                    break;
                case 'stats':
                    store.commit("setSideA",{id: 3,name: i18n.t('sidemenuUser.opt3'), route:'stats',active: true, disabled: false,icon:"far fa-chart-bar"})
                    next();
                    break;
                default:
                    next();
                    break;
            } 
        }
    } 
    else{
        console.log('entro al else');
        console.log(store.getters.logged);
        if (store.getters.logged == true) // si si esta loggeado lo manda al projects
        {
            console.log('entro a redirigir');
            next({ name: 'projects', params: {lang: i18n.locale} });
        } 
        else{
            next();
        }
        
    }
    

})

export default myrouter;
