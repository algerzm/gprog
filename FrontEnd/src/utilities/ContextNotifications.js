
import store from "../store";
import i18n from '../i18n'
import Vue from 'vue'
//import store from '../store';

function getMessage(error, module,type){
    var err=error.response.status;
    var message={text:null,title:null};
    if(type =='pleasant'){
        switch (err) {//IDENTIFICA EL TIPO DE ERROR PARA TARJETA AGRADABLE
            case 404:
                message.title=i18n.t('context.no')+module+i18n.t('context.yet')
                if(module != 'projects'){
                    message.text=i18n.t('context.add')+module+"!"
                }
                else{
                    message.text=i18n.t('context.proj')
                }
                
                break;
            case 409:
                message.title="Oops..."
                message.text=i18n.t('context.409')
                break;
            case 401:
                message.title="Oops..."
                message.text=i18n.t('context.401')
                break;
            default: 
                message.text="";
                break;
        }
    }
    else
    {
        message.title="Error: "+err;
        message.text=error.response.data;   
    }
    return message;

}
/*
user: {
    nombre,
    correo,
    password,
    image,
    preferences: {
        theme: 'default',
        lang: 'en'
        contextNofifications: true /false
    }
}
*/
function Notify(){}

Notify.prototype.warning = function(module,error){
    if(store.getters.user.preferencias.notificaciones == true){
        var message=getMessage(error,module,'pleasant');
        Vue.notify({
            group: 'pleasant',
            title: message.title,
            text: message.text,
            type: 'warn'
          })
    }

}

Notify.prototype.success = function(title,text){
    if(store.getters.user.preferencias.notificaciones == true){
        Vue.notify({
            group: 'pleasant',
            title: title,
            text: text,
            type: 'success'
          })
    }

}

Notify.prototype.error = function(title, text){
    Vue.notify({
        group: 'foo',
        title: title,
        text: text,
        type: 'error'
      })
}
const noti = new Notify()



export default noti;