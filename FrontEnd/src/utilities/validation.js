
import Vue from 'vue'
/* eslint-disable no-unused-vars */
import { configure } from 'vee-validate';
import { required, email, integer, decimal,max_value,min_value } from 'vee-validate/dist/rules';
import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate';
/* eslint-enable no-unused-vars */


configure({
  classes: {
    valid: 'is-valid',
    invalid: 'is-invalid',
    dirty: ['is-dirty', 'is-dirty'], // multiple classes per flag!
    // ...
  }
})
extend('max_value', max_value);
extend('min_value', min_value);


extend("decimal", {
    validate: (value, { decimals = '*', separator = '.' } = {}) => {
      if (value === null || value === undefined || value === '') {
        return {
          valid: false
        };
      }
      if (Number(decimals) === 0) {
        return {
          valid: /^-?\d*$/.test(value),
        };
      }
      const regexPart = decimals === '*' ? '+' : `{1,${decimals}}`;
      const regex = new RegExp(`^[-+]?\\d*(\\${separator}\\d${regexPart})?([eE]{1}[-]?\\d+)?$`);
      return {
        valid: regex.test(value),
        data: {
          serverMessage: 'Only decimal values are available'
        }
      };
    },
    message:    'Only decimal values are valid'
  })

extend('integer', integer);
// No message specified.
//extend('email', email);
extend('email', {
    ...email,
    message: `Make sure it's written correctly`
  });

// Override the default message.
extend('required', {
  ...required,
  message: 'This field is required'
});

extend('odd', value => {
    return value % 2 !== 0;
  });
extend('positive', value => {
  return value >= 0;
});

Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
