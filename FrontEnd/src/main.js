import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import Notifications from 'vue-notification'

Vue.use(Notifications);
Vue.config.productionTip = false;
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import router from "./router";
import store from "./store";
import i18n from './i18n';

/* eslint-disable no-unused-vars */
import validation from './utilities/validation'
import 'cropperjs/dist/cropper.css';

/* eslint-enable no-unused-vars */

document.title= 'Gprog';
new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
