const signalR = require('@aspnet/signalr');
//import api from '../api/api';
import store from '../store';

const connection = new signalR.HubConnectionBuilder()
  .withUrl('https://gapi.montza.com/notificationHub')
  .build();

connection.start().catch(function (err) {
  return console.error(err.toString());
});

connection.on('ReceiveNotification', async function (title, mess, idnoti) {
  store.commit('addNoti', { id: idnoti, title: title, message: mess });
});

export default connection;
