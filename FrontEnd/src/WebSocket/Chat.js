const signalR = require('@aspnet/signalr');
//import store from "../store";

const connection = new signalR.HubConnectionBuilder()
  .withUrl('https://gapi.montza.com/chatHub')
  .build();

export default connection;
