const signalR = require('@aspnet/signalr');
//import api from '../api/api';
//import store from "../store";

const connection = new signalR.HubConnectionBuilder()
  .withUrl('https://gapi.montza.com/menuHub')
  .build();

connection.start().catch(function (err) {
  return console.error(err.toString());
});

export default connection;
